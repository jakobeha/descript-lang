{-# LANGUAGE DeriveDataTypeable #-}

module Core.Test.HUnit
  ( tryTest
  , denoteFailIn
  , denoteFail
  ) where

import Test.HUnit.Lang
import Data.Maybe
import Control.Exception
import Type.Reflection

-- | An arbitrary exception with a prefix in its description.
data SomePrefixedException e
  = SomePrefixedException String e
  deriving (Typeable)

instance (Show e) => Show (SomePrefixedException e) where
  show (SomePrefixedException pre exc)
    = addPrefixToMsg pre $ show exc

instance (Exception e) => Exception (SomePrefixedException e) where
  displayException (SomePrefixedException pre exc)
    = addPrefixToMsg pre $ displayException exc

-- | Will return 'Nothing' if an exception was raised.
tryTest :: IO a -> IO (Maybe a)
tryTest action = do
  res <- try action
  case res of
       Left HUnitFailure{} -> pure Nothing
       Right val -> pure $ Just val

-- | If a failure occurs, denotes that it occurred for "in" the given label.
denoteFailIn :: String -> IO a -> IO a
denoteFailIn label = denoteFail $ "in " ++ label ++ ": "

-- | If a failure occurs, prepends the given string to the error
-- message.
denoteFail :: String -> IO a -> IO a
denoteFail pre test = test `catches` rethrowWithPrefix pre

-- | Prepends the given string to the error message.
rethrowWithPrefix :: String -> [Handler a]
rethrowWithPrefix pre =
  [ Handler $ rethrowHUnitWithPrefix pre
  , Handler $ rethrowGenWithPrefix pre
  ]

rethrowHUnitWithPrefix :: String -> HUnitFailure -> IO a
rethrowHUnitWithPrefix pre = throw . addPrefixToFailure pre

rethrowGenWithPrefix :: String -> SomeException -> IO a
rethrowGenWithPrefix pre = throw . addPrefixToGenException pre

addPrefixToFailure :: String -> HUnitFailure -> HUnitFailure
addPrefixToFailure pre (HUnitFailure srcLoc reason)
  = HUnitFailure srcLoc $ addPrefixToReason pre reason

addPrefixToGenException :: (Exception e) => String -> e -> SomePrefixedException e
addPrefixToGenException pre exc = SomePrefixedException pre exc

addPrefixToReason :: String -> FailureReason -> FailureReason
addPrefixToReason pre (Reason msg) = Reason $ addPrefixToMsg pre msg
addPrefixToReason pre (ExpectedButGot extraMsg expected got)
  = ExpectedButGot (addPrefixToExtraMsg pre extraMsg) expected got

addPrefixToExtraMsg :: String -> Maybe String -> Maybe String
addPrefixToExtraMsg pre = Just . addPrefixToMsg pre . fromMaybe ""

addPrefixToMsg :: String -> String -> String
addPrefixToMsg pre msg = pre ++ msg
