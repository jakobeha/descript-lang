# Documentation

How this documentation is (roughly) organized:

- `specification`: Contains information about the Descript language
  itself, like syntax, semantica, and conventions.
- `implementation`: Contains information about the Descript compiler,
  and how the Descript language is interpreted.
