# Descript v0.3

## Idea

- A language which is:
  - Incredibly simple: Descript v0.2, but
    - Multiple record types can have the same name as long as they have different property keys - record types are distinguished by names *and* property keys
    - No unions.
  - Very expressive: e.g. if you want to create the program "calculate 2 + 2", you could write `Add[a: 2, b: 2]`, `Add[left: 2, right: 2]`, `Add[left: Two[], right: Two[]]`, `Calc[expr: "2 + 2"]`, `Calc[expr: Parse["2 + 2"]]`, `Calculate[expr: Operation[operator: Add[], left: 2, right: 2]]` see [[Goals]]).
  - Very clear (easy to read): because of expressivity and conventions.
  - Very strict: no inference, no defaults (no more unions), *except* with syntactic sugar which is extra and goes away.
  - Very verbose (hard to write): partly because it's strict, partly because it's easy to read / conventions. *Minimized* by syntactic sugar which is extra and goes away.
  - *Very easy for a computer to process.*
- Lots of tools to auto-generate and refactor code in this language.
  - Ability to perform quick refactors in individual files, quick enough so code-generation tools can be used alongside typing code (maybe like autocompletion, Haml)
  - Ability to perform refactors over entire workspaces (lots of files).
  - Later, might include some tools to refactor code just when its viewed (so you can see code "from a certain perspective").

Ideally, the tools will make the language's strictness and verbosity manageable. In practice, the tools are going to be very hard to implement.

## Steps to Implement

In order, but later steps will be started before earlier steps are finished, and carried out with earlier steps.

- Start with Descript v0.2, apply these changes:
  - Allow more general reducers - these changes specifically benefit refactor reducers (macro reducers which are applied to all values in a file to refactor it)
    - Allow input values to just consume primitive types, and allow them to consume any value with `#Any[]`. In the implementation, this means input values are `OptValue`s (might want to rename `OptValue`  make it more clear that `NothingValue` is a free bind - call them `MatchValue` and `BindValue`.
    - Allow output values/property paths to reference the entire reducer with `#self`. In the implementation, this means property paths can have 0 elements (regular lists, not empty) and there's no need for an explicit subpath type.
    - For example, these changes would allow `#Number[]: Num[a: self]` and `#Any[]: Typed[value: self, type: Unknown[]]`.
  - Allow records with the same name but different properties. Make `(Record)Head` an explicit type, and have it include property keys when checking for equality. This approach should ensure that programs which use the "wrong" records and properties (e.g. `Add[foo]` when only `Add[left, right]` is declared) never crash interpretation and still fail validation. Also need to modify validation, desugaring, and maybe refactoring code for this change.
  - Remove unions, probably when they start getting in the way of implementing tools.
- Come up with refactor tools, and general definition of what "refactoring" will be.
  - ... TODO Do this and add to docs, this is important.
- Add some library (DSL? In Descript?) to analyze and manipulate Descript and create refactor tools.
  - ... TODO How? Probably via refactor reducers, and improving the macro system so they're more powerful, e.g. adding macros which act on and create arbitrary-labeled symbols.
- Implement some refactor tools.
- Create a VSCode extension which wraps around the refactor tools.

### Examples of Refactors

- **Rename** - record head, property key, import path. Can be local (only affects the current source, changes renaming in import declarations), or can also affect dependents (importing modules). The operation itself *won't* affect dependencies (imported modules), but it can be performed on a dependency from a dependent - first the symbol is resolved to its original source (in the dependency), then the refactor is performed on the original source (affecting dependents, including the one the refactor "came from").
- **Vary** (convert into variant). Examples: convert all numbers into `Number[#self]`, convert all `JPEG[a]` and `PNG[a]` into `Image[JPEG[a]]` and `Image[PNG[a]]`, convert all values into `Typed[value: #self, type: Unknown[]]`
- **Refactor reduce** (instant edit)
  - Generalizes: rename records and property keys (but not imports), convert into variant

#### Instant Edit

While editing a Descript program, a reducer can be applied to all values in a source file. To do this:

1. Ensure that the source has valid syntax - specifically, top-level forms can be parsed from it, although they don't need to be in order (e.g. the query could come before record declarations) and they don't need to be semantically valid (e.g. a record can have the wrong property keys).
2. (Keeping valid syntax) insert the reducer anywhere in the top level of the file, suffix the reducer with `!`, and press "enter" (creating a newline).
3. The file will be modified - the reducer will be removed from the
   file, and it'll be applied to all values in the file so they get
   modified.
