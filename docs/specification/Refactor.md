# Refactor

## Instant Edit

While editing a Descript program, a reducer can be applied to all values in a source file. To do this:

1. Ensure that the source has valid syntax - specifically, top-level forms can be parsed from it, although they don't need to be in order (e.g. the query could come before record declarations) and they don't need to be semantically valid (e.g. a record can have the wrong property keys).
2. (Keeping valid syntax) insert the reducer anywhere in the top level of the file, suffix the reducer with `!`, and press "enter" (creating a newline).
3. The file will be modified - the reducer will be removed from the file, and it'll be applied to all values in the file so they get modified.
