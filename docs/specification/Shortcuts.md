# Shortcuts

Shortcuts are a special form of syntax sugar. Every shortcut begins with
an `$`. Like other syntax sugar, shortcuts are entirely for convenience,
and they expand into regular values.

> The expanded values contain symbols for "primitive" records (records
> defined in `Base>Primitive`), like `Nil` and `Apply1`. However, the
> symbols don't have attached scopes. So if another definition of these
> records is defined, the shortcuts will use that definition, and if no
> definition is defined, they'll generate "undeclared record head"
> validation errors.

## List of Shortcuts

The following is a rough specification for shortcuts. In general:

```descript
...<pre>
$<x> => <y>
...<extra>
```

specifies that a shortcut like the example `$<x>` will expand into
`<y>`. `...<pre>` may contain record declarations and reducers, and if
so, the expansion will only happen if those record declarations and
reducers are already in the module's context.`...<extra>` may contain
record declarations and reducers, and if so, those will be added to the
module's context after expansion.

Also note that `{foo}` (not following `$`) is a symbol, e.g. it can
exist in:

```descript
{foo}[].

{foo}[]: "Bar"

{foo}[]?
```

The `{` and `}` denote that the symbol is created by desugaring, and
make it impossible for a user to directly declare or use it (since user
symbols must start with a letter or `#`).

---

### Lists

```descript
$[foo, bar, baz] => Cons[foo, Cons[bar, Cons[baz, Nil[]]]]
```

### First-class Functions (TODO)

```descript
Foo[foo].
$\Foo => {\Foo}
{\Foo}[].
Apply1[f: {\Foo}, a]: {\Foo}[foo: a]
```

```descript
Foo[foo, bar].
$\Foo => {\Foo}
{\Foo}[].
Apply2[f: {\Foo}, a, b]: {\Foo}[foo: a, bar: b]
```

#### Property Lambdas (TODO)

```descript
Foo[foo].
$\>foo<Foo => {\>foo<Foo} //Must have the `<` to identify the record
{\>foo<Foo}[].
Apply1[f: {\>foo<Foo}[], a: Foo[foo]]: a>foo<Foo
```

```descript
Foo[foo, bar].
Bar[foo, bar].
$\>foo<Foo>bar<Bar => {\>foo<Foo>bar<Bar} //Must have the `<`s
{\>foo<Foo>bar<Bar}[].
Apply1[f: {\>foo<Foo>bar<Bar}[], a: Foo[foo: Bar[foo, bar], bar]]:
  a>foo<Foo>bar<Bar
```
