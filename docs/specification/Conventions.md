# Conventions

Descript is a very simple language, with a few general "rules". It's
expressive, in that there are a lot of different ways to build and
structure programs. Unfortunately, there are a lot of "wrong" ways to
structure programs, so that such programs aren't readable or extendible.

So besides rules, Descript also has **conventions** (design patterns).
These conventions make it easier for users (and in the future, the
interpreter) to understand code and find bugs. They make it possible to
extend code (abstract, add new features, etc.) without rewriting. And
they're consistent, which allows you (and in the future, the
interpreter) to read and write code easier - thinking with "algorithms"
instead of "heuristics".

---

## 1 value = 1 type of information, many types of data

Descript supports arbitrarily combining many values into one - given any
values `A`, `B`, `C`, `...`, you can create one value `A | B | C | ...`.
*Only combine values if they both represent the same thing. Don't make a
value representing one piece of information part of another value.*

### Bad

Consider a set of items, for example:

```descript
ornaments: Ball[] | Star[] | Light[]
```

But a set and it's items are separate pieces of information. `ornaments`
doesn't encode a ball, a star, or a light - it encodes a set which
contains those things. The problem is that `ornaments` will be processed
the same as a ball, star, and light.

This might be useful sometimes, like with the reducers:

```descript
Hang[a: Ball[]]: HungBall[]
Hang[a: Star[]]: HungStar[]
Hang[a: Light[]]: HungLight[]
```

Using the above code, `Hang[ornaments]` will produce `HungBall[] |
HungStar[] | HungLight[]` - a set of hung ornaments, as the programmer
(who hypothetically wrote the code) probably expected.

However, it won't work with all methods. Consider:

```descript
Hang[a]: Hung[a]
```

This will reduce `ornaments` to `Hung[Ball[] | Star[] | Lights[]]`, when
the programmer could've expected `Hung[Ball[]] | Hung[Star[]] |
Hung[Lights[]]`. In fact, `A[...] | A[...]` can never be a value, it
would become `A[... | ...]` - a value can only contain one instance of a
record type, and multiple instances will have their contents merged.

Also consider:

```descript
Hang[a: Ball[]]: Code[
  lang: "holidayScript"
  content: "move to middle of tree, add string, add ball"
]
Hang[a: Star[]]: Code[
  lang: "holidayScript"
  content: "move to top of tree, add star"
]
Hang[a: Light[]]: Code[
  lang: "holidayScript"
  content: "add light start, move around tree, add light end"
]

Hang[ornaments]?
```

This will produce `Code[...] | Code[...] | Code[...]`, which will *not*
compile. The programmer might've expected Descript to interpret a union
of `Code`s as a set of script fragments which should be appended to form
the final program. But instead, Descript roughly "interprets" the
`Code`s as many representations of a single program. Then it doesn't
know which one to choose, so it fails. A programmer following "1 value =
1 type of information" would do the same.

#### Correct alternative

To represent a set of items, use an explicit record, `Set`:

```descript
ornaments: Set[$[Ball[], Star[], Light[]]]
```

### Good

One value can store multiple encodings:

```descript
music: MPEG4[data: ...] | M4V[data: ...]
```

`music` can be interpreted as either an `MPEG4` or an `M4V`. And
Descript will interpret it as both.

```descript
ShiftPitch[a: MPEG4[data], amount: #Number[]]: MPEG4[...<altered>]
ShiftPitch[a: M4V[data], amount: #Number[]]: M4V[...<altered>]

ShiftPitch[music]: MPEG4[data: ...<altered>] | M4V[data: ...<altered>]
```

#### Types

Types are another example of an "alternate representation" of a single
piece of information. Consider:

```descript
three: 3 | Integer[]
```

In this case, the type is considered a "more general" representation of
the value.

Types can make reduction failures clearer and prevent unexpected
reduction successes. There are 2 ways to use types:

1. Define separate reducers for the underlying value and type. For
   example:

```descript
Fib[a: Integer[]]: Integer[]
Fib[a: #Number[]]: Divide[Add[Exp[Add[1, Sqrt[5]], a], Exp[Sub[1, Sqrt[5]], a]], Sqrt[5]]
```

Whenever a typed value is passed to a reducer like the example above,
the reducer will need to reduce both the value and the type. If the
reducer can't reduce the type, even if it can reduce the underlying
value, it will get stuck and generate a reduction error which clearly
shows (both to the user and in the future, the interpreter) a type
mismatch. Continuing the above example:

```descript
Fib[4 | Integer[]]: 3 | Integer[]
Fib[3.5 | Decimal[]]: ... | Fib[Decimal[]] //Fib[Decimal[]] shows that Fib isn't intended for Decimals, even though the number itself reduces
```

2. Define reducers which operate on both a value and it's type, *and*
   wrap the value in `Value[a]`. For example:

```descript
Fib[a: Value[a: #Number[]] | Integer[]]: Divide[Add[Exp[Add[1, Sqrt[5]], a>a], Exp[Sub[1, Sqrt[5]], a>a]], Sqrt[5]]
```

The value needs to be wrapped if it could be arbitrary, because a
reducer can't match an arbitrary value with another value. For example,
there's no way to express the following:

```descript
Foo[a: <any> | FooType[]]: Bar[<a without FooType[]>]
```

However, if `<any>` is wrapped, it can be matched:

```descript
Foo[a: Value[a] | FooType[]]: Bar[a>a]
```

* Optionally, also wrap the type in `Type[a]`, for style and
  consistency.

---

In general, if you add or remove part of a value, it shouldn't change
the value's meaning, it should only add or remove ambiguity.
