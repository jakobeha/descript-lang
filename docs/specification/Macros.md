# Macros

In Descript, a **macro reducer** (or **macro**) is a reducer which gets
applied to other reducers - it transforms the input and output values
within those reducers. The reducers in a Descript module are grouped
into multiple **phases**, separated by `---`, and a reducer is applied
to the reducers in every phase below its own phase (after it's
transformed by reducers in above phases). Macros allow you to abstract
code inside of reducers, in particular input values.

(Note that record declarations apply to all phases, and are only used to
validate the program, they don't affect how it's interpreted or
evaluated. The query is transformed by reducers in all phases).

## Example

### Initial design

Consider this program, a simple API for JavaScript:

```descript
import Base{Code, List, Nil, Cons, Map, Concat, Action, ToString, Quote}

DeclVar[id].
GetVar[id].
SetVar[id, new].
Num[a].
Str[a].
Add[l, r].
Log[msg].
Prgm[stmts].
GetCodeContent[].

//Phase 0 (reducers)
DeclVar[id: #String[]]: Code[
  lang: "JavaScript"
  content: Concat[$["var ", id, ";"]]
]
GetVar[id: #String[]]: Code[
  lang: "JavaScript"
  content: id
]
SetVar[id: #String[], new: Code[lang: "JavaScript", content]]: Code[
  lang: "JavaScript"
  content: Concat[$[id, " = ", new>content, ";"]]
]
Num[a: #Number[]]: Code[
  lang: "JavaScript"
  content: ToString[a]
]
Str[a: #String[]]: Code[
  lang: "JavaScript"
  content: Quote[a]
]
Add[
  l: Code[lang: "JavaScript", content]
  r: Code[lang: "JavaScript", content]
]: Code[
  lang: "JavaScript"
  content: Concat[$[l>content, " + ", r>content]]
]
Log[msg: Code[lang: "JavaScript", content]]: Code[
  lang: "JavaScript"
  content: Concat[$["console.log(", msg>content, ");"]]
]
Prgm[stmts]: Code[
  lang: "JavaScript"
  content: Concat[Map[GetCodeContent[], stmts]]
]
Action[f: GetCodeContent[], a: Code[lang, content]]: a>content

Prgm[$[
  DeclVar["foo"]
  SetVar["foo", Num[3]]
  Log[Add[Str["foo is "], GetVar["foo"]]]
  SetVar["foo", Add[Num[3], Num[2]]]
  Log[Add[Str["now foo is "], GetVar["foo"]]]
]
```

There isn't any phase separator `---`, so all of the program's reducers
are in "phase 0", and they aren't applied to each other. The program
evaluates to:

```descript
Code[
  lang: "JavaScript"
  content: "var foo;foo = 3;console.log(\"foo is \" + foo);foo = 3 + 2;console.log(\"now foo is \" + foo);"
]
```

which compiles to:

```javascript
var foo;
foo = 3;
console.log("foo is " + foo);
foo = 3 + 2;
console.log("now foo is " + foo);
```

which produces the following output:

```log
foo is 3
foo is 5
```

### Refactor

In the above, the record `Code[lang: "JavaScript", content]` is repeated
many times. It can be abstracted into `JS[code]`, so that
`JS[code: ...]` is semantically equivalent to `Code[lang: "JavaScript",
content: ...]`. This is done with a reducer:

```descript
JS[code]: Code[lang: "JavaScript", content: code]
```

However, the position of the reducer is important, because it can't just
be an ordinary reducer - it must be a macro.

#### Invalid

The following *won't* work:

```descript
import Base{Code, List, Nil, Cons, Map, Concat, Action, ToString, Quote}

DeclVar[id].
GetVar[id].
SetVar[id, new].
Num[a].
Str[a].
Add[l, r].
Log[msg].
Prgm[stmts].
GetCodeContent[].
JS[code].

//Phase 0

//New reducer - *declared in phase 0*
JS[code]: Code[lang: "JavaScript", content: code]

DeclVar[id: #String[]]: JS[Concat[$["var ", id, ";"]]]
GetVar[id: #String[]]: JS[id]
SetVar[id: #String[], new: JS[code]]: JS[Concat[$[id, " = ", new>code, ";"]]]
Num[a: #Number[]]: JS[ToString[a]]
Str[a: #String[]]: JS[Quote[a]]
Add[l: JS[code], r: JS[code]]: JS[Concat[$[l>code, " + ", r>code]]]
Log[msg: JS[code]]: JS[Concat[$["console.log(", msg>code, ");"]]]
Prgm[stmts]: JS[Concat[Map[GetCodeContent[], stmts]]]
Action[f: GetCodeContent[], a: JS[code]]: a>code

Prgm[$[
  DeclVar["foo"]
  SetVar["foo", Num[3]]
  Log[Add[Str["foo is "], GetVar["foo"]]]
  SetVar["foo", Add[Num[3], Num[2]]]
  Log[Add[Str["now foo is "], GetVar["foo"]]]
]
```

It evaluates to:

```descript
Prgm[$[
  Code[lang: "JavaScript", content: "var foo;"]
  SetVar["foo", Code[lang: "JavaScript", content: "3"]]
  Log[Add[
    Code[lang: "JavaScript", content: "\"foo is \""]
    Code[lang: "JavaScript", content: "foo"]
  ]]
  SetVar["foo", Add[
    Code[lang: "JavaScript", content: "3"]
    Code[lang: "JavaScript", content: "2"]
  ]]
  Log[Add[
    Code[lang: "JavaScript", content: "\"now foo is \""]
    Code[lang: "JavaScript", content: "foo"]
  ]]
]]
```

which fails to compile.

In particular, it gets stuck interpreting reducers which take in `JS`
records, like `SetVar`, even though they were originally applied with
`JS` records.

The problem is that the reducer `JS[code]: Code[lang: "JavaScript",
content]` is applied to values inside the query, but it isn't applied to
the input values in the other reducers. Reducers like `SetVar` still
expect unreduced `JS` records, but (as shown in the bad result) they're
applied with already-reduced `Code` records, so they fail to reduce.

#### Working

The solution is to make `JS[code]: Code[lang: "JavaScript", code]`
a macro. Then it gets applied to the reducers, so they expect a
`Code[lang: "JavaScript", content]`. The following works:

```descript
import Base{Code, List, Nil, Cons, Map, Concat, Action, ToString, Quote}

DeclVar[id].
GetVar[id].
SetVar[id, new].
Num[a].
Str[a].
Add[l, r].
Log[msg].
Prgm[stmts].
GetCodeContent[].
JS[code].

//Phase 1

//New reducer - *declared in phase 1*
JS[code]: Code[lang: "JavaScript", content: code]

--- //Lowers to phase 0

//Phase 0
DeclVar[id: #String[]]: JS[Concat[$["var ", id, ";"]]]
GetVar[id: #String[]]: JS[id]
SetVar[id: #String[], new: JS[code]]: JS[Concat[$[id, " = ", new>code, ";"]]]
Num[a: #Number[]]: JS[ToString[a]]
Str[a: #String[]]: JS[Quote[a]]
Add[l: JS[code], r: JS[code]]: JS[Concat[$[l>code, " + ", r>code]]]
Log[msg: JS[code]]: JS[Concat[$["console.log(", msg>code, ");"]]]
Prgm[stmts]: JS[Concat[Map[GetCodeContent[], stmts]]]
Action[f: GetCodeContent[], a: JS[code]]: a>code

Prgm[$[
  DeclVar["foo"]
  SetVar["foo", Num[3]]
  Log[Add[Str["foo is "], GetVar["foo"]]]
  SetVar["foo", Add[Num[3], Num[2]]]
  Log[Add[Str["now foo is "], GetVar["foo"]]]
]
```

It evaluates like the first example, and produces:

```descript
Code[
  lang: "JavaScript"
  content: "var foo;foo = 3;console.log(\"foo is \" + foo);foo = 3 + 2;console.log(\"now foo is \" + foo);"
]
```

which compiles to:

```javascript
var foo;
foo = 3;
console.log("foo is " + foo);
foo = 3 + 2;
console.log("now foo is " + foo);
```

which produces the following output:

```log
foo is 3
foo is 5
```

The key difference is the `---`, which puts the `JS[code]: Code[lang:
"JavaScript", content: code]` reducer (above) in phase 1, and the other
reducers (below) in phase 0.

## Instant Edit

Macro reducers can also be applied directly to a file, altering the
source code. These are known as **refactor reducers**. See "Instant
Edit" in [[Refactor]] for how to use them.
