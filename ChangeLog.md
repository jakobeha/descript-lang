# Revision history for descript-lang

## 0.1.0  -- 2017-05-29

## 0.2.0  -- 2018-04-09

- Starting almost from Scratch. Keeping the same syntax, name, general
  concept / philosophy, and language. Getting rid of the "logical"
  idea where when a value implies something else, it still "remembers" its old self -
  e.g. if `a: b` and `a: c`, before `a: b | c`, now you would get an ambiguity error.
  That made the time complexity way too high.

## 0.2.1 -- 2018-04-23

- Got the CLI able to interpret and print out (evaluate) simple
  programs.

## 0.2.2 -- 2018-05-01

- Got the core language working:
  - Parses
  - Validates
  - Interprets
  - Evaluates
  - Compiles
  - Reprints
  - Refactors (currently just renames record heads)

## 0.2.3 -- 2018-05-11

- Added a basic language server, a few bugs but usable.

## 0.2.4 -- 2018-05-17

- Added importing and dependencies
- Refactored build model.

## 0.2.5 -- 2018-06-04

- Added phase-shifting imports
- Added some syntax sugar
- Expanded base library
- Refactored importing and dependencies
