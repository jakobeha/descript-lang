-- | Encodes syntax sugar. Currently:
-- - Implicit record properties (resolved by index)
-- - Implicit path key heads (resolved by input)
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
