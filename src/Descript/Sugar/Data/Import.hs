module Descript.Sugar.Data.Import
  ( module Descript.BasicInj.Data.Import
  , mkImportCtx
  ) where

import Descript.BasicInj.Data.Import
import Descript.Misc

-- | If a module declaration isn't given, uses the default (file's)
-- module scope.
mkImportCtx :: (TaintAnn an)
            => SFile
            -> an
            -> Maybe (ModuleDecl an)
            -> [AbsScope -> ImportDecl an]
            -> ImportCtx an
-- TODO Find better solution - @AbsScope -> ImportDecl an@ is ugly.
mkImportCtx file' ann Nothing sfreeIDecls = ImportCtx ann mdecl idecls
  where idecls = map ($ moduleDeclScope mdecl) sfreeIDecls
        mdecl = preInsertAnn ann <$ defModuleDecl file'
mkImportCtx _ ann (Just mdecl) sfreeIDecls = ImportCtx ann mdecl idecls
  where idecls = map ($ moduleDeclScope mdecl) sfreeIDecls
