{-# LANGUAGE TupleSections #-}

module Descript.Sugar.Parse.Refine
  ( freeToImportDeclIn
  , freeToRecordDecl
  , freeToReducer
  , freeToQuery
  , freeToRegValue
  , freeToInput
  , freeToOutput
  , freeToRecordType
  ) where

import Descript.Sugar.Data.Source
import Descript.Sugar.Data.Import
import Descript.Sugar.Data.Reducer
import qualified Descript.Sugar.Data.Value.In as In
import qualified Descript.Sugar.Data.Value.Out as Out
import qualified Descript.Sugar.Data.Value.Reg as Reg
import Descript.Sugar.Data.Value.Gen
import Descript.Sugar.Data.Type
import Descript.Sugar.Data.Atom
import qualified Descript.Free as Free
import Descript.Misc

freeToImportDeclIn :: (TaintAnn an)
                   => AbsScope
                   -> Free.ImportDecl an
                   -> ImportDecl an
freeToImportDeclIn scope (Free.ImportDecl ann path ups isrcs idsts)
  = ImportDecl
  { importDeclAnn = ann
  , importDeclPath = path
  , importDeclPhaseUps = ups
  , importDeclSrcImports = map (freeToImportRecordIn scope pscope) isrcs
  , importDeclDstImports = map (freeToImportRecordIn scope pscope) idsts
  }
  where pscope = FSymbolScope (phaseUpsNum ups) $ modulePathScope path

freeToImportRecordIn :: (TaintAnn an)
                     => AbsScope
                     -> FSymbolScope
                     -> Free.ImportRecord an
                     -> ImportRecord an
freeToImportRecordIn scope pscope (Free.ImportRecord ann from to)
  = ImportRecord ann from' to'
  where from' = FSymbol pscope from
        to' = mkFSymbol scope to

-- | Refines to a record declaration.
freeToRecordDecl :: Free.RecordDecl Range -> Free.RefineResult (RecordDecl Range)
freeToRecordDecl (Free.RecordDecl ann recordType) = RecordDecl ann <$> freeToRecordType recordType

-- | Refines to a reducer.
freeToReducer :: Free.Reducer Range -> Free.RefineResult (Reducer Range)
freeToReducer (Free.Reducer ann input' output')
  = Reducer ann <$> freeToInput input' <*> freeToOutput output'

-- | Refines to a query.
freeToQuery :: Free.Query Range -> Free.RefineResult (Query Range)
freeToQuery (Free.Query ann val) = Query ann <$> freeToRegValue val

-- | Refines to a regular value.
freeToRegValue :: Free.Value Range -> Free.RefineResult (Reg.Value Range)
freeToRegValue (Free.Value ann parts) = Value ann <$> freeToParts parts

-- | Refines to regular parts.
freeToParts :: [Free.Part Range] -> Free.RefineResult [Reg.Part Range]
freeToParts = traverse freeToPart

-- | Refines to a regular part.
freeToPart :: Free.Part Range -> Free.RefineResult (Reg.Part Range)
freeToPart (Free.PartPrim prim) = Success $ Reg.PartPrim prim
freeToPart (Free.PartRecord record) = Reg.PartRecord <$> freeToRecord record
freeToPart x@(Free.PartPropPath (PropPath ann _))
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "primitive or record"
  , Free.actual = "property path"
  , Free.actualPr = pprintStr x
  }
freeToPart (Free.PartShortcut shortcut) = freeShortcutToPart shortcut

-- | Refines to a regular record.
freeToRecord :: Free.Record Range -> Free.RefineResult (Reg.Record Range)
freeToRecord record
    = Record (Free.recordAnn record) (Free.head record)
  <$> freeToProperties (Free.properties record)

-- | Refines to regular properties.
freeToProperties :: [Free.Property Range] -> Free.RefineResult [Reg.Property Range]
freeToProperties = traverse freeToProperty

-- | Refines to a regular property.
freeToProperty :: Free.Property Range -> Free.RefineResult (Reg.Property Range)
freeToProperty (Free.PropertySingle val)
  = Property ann Nothing <$> freeToRegValue val
  where ann = getAnn val
freeToProperty (Free.PropertyDef ann key val)
  = Property ann (Just key) <$> freeToRegValue val

-- | Refines a shortcut to a regular shortcut.
freeShortcutToPart :: Free.Shortcut Range -> Free.RefineResult (Reg.Part Range)
freeShortcutToPart (Free.ShortList ann items)
  = Reg.PartRecord <$> freeToRecord (Free.expandShortList ann items)

-- | Refines to Range input value.
freeToInput :: Free.Value Range -> Free.RefineResult (In.Value Range)
freeToInput (Free.Value ann parts) = Value ann <$> freeToInParts parts

-- | Refines to input parts.
freeToInParts :: [Free.Part Range] -> Free.RefineResult [In.Part Range]
freeToInParts = traverse freeToInPart

-- | Refines to Range input part.
freeToInPart :: Free.Part Range -> Free.RefineResult (In.Part Range)
freeToInPart (Free.PartPrim prim) = Success $ In.PartPrim prim
freeToInPart (Free.PartRecord record)
  | isFreePrimType record = In.PartPrimType <$> freeToPrimType record
  | otherwise = In.PartRecord <$> freeToInRecord record
freeToInPart x@(Free.PartPropPath (PropPath ann _))
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "primitive, primitive type, or record"
  , Free.actual = "property path"
  , Free.actualPr = pprintStr x
  }
freeToInPart (Free.PartShortcut shortcut)
  = freeShortcutToInPart shortcut

isFreePrimType :: Free.Record Range -> Bool
isFreePrimType = isInjSymbol . Free.head

freeToPrimType :: Free.Record Range -> Free.RefineResult (PrimType Range)
freeToPrimType record
  | null $ Free.properties record = freeSymbolToPrimType (Free.recordAnn record) $ Free.head record
  | otherwise = Failure $ Free.entireRefineDiff (Free.recordAnn record) Free.LocalRefineDiff
  { Free.expected = "primitive type"
  , Free.actual = "injected function application"
  , Free.actualPr = pprintStr record
  }

freeSymbolToPrimType :: Range -> Symbol Range -> Free.RefineResult (PrimType Range)
freeSymbolToPrimType ann (Symbol _ label) = freeSymbolStrToPrimType ann label

freeSymbolStrToPrimType :: Range -> String -> Free.RefineResult (PrimType Range)
freeSymbolStrToPrimType ann "#Number" = Success $ PrimTypeNumber ann
freeSymbolStrToPrimType ann "#String" = Success $ PrimTypeText ann
freeSymbolStrToPrimType ann x
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "#Number or #String"
  , Free.actual = "non-existent primitive type"
  , Free.actualPr = x
  }

-- | Refines to Range input record.
freeToInRecord :: Free.Record Range -> Free.RefineResult (In.Record Range)
freeToInRecord record
    = Record (Free.recordAnn record) (Free.head record)
  <$> freeToInProperties (Free.properties record)

-- | Refines to input properties.
freeToInProperties :: [Free.Property Range] -> Free.RefineResult [In.Property Range]
freeToInProperties = traverse freeToInProperty

-- | Refines to Range input property.
freeToInProperty :: Free.Property Range -> Free.RefineResult (In.Property Range)
freeToInProperty (Free.PropertySingle x)
  = case Free.valueToPropKey x of
         Nothing -> Property ann Nothing . In.JustValue <$> freeToInput x
         Just key -> Success $ Property ann (Just key) In.NothingValue
  where ann = getAnn x
freeToInProperty (Free.PropertyDef ann key val)
  = Property ann (Just key) . In.JustValue <$> freeToInput val

-- | Refines a shortcut to an input part.
freeShortcutToInPart :: Free.Shortcut Range -> Free.RefineResult (In.Part Range)
freeShortcutToInPart (Free.ShortList ann items)
  = In.PartRecord <$> freeToInRecord (Free.expandShortList ann items)

-- | Refines to Range output value.
freeToOutput :: Free.Value Range -> Free.RefineResult (Out.Value Range)
freeToOutput (Free.Value ann parts) = Value ann <$> freeToOutParts parts

-- | Refines to output parts.
freeToOutParts :: [Free.Part Range] -> Free.RefineResult [Out.Part Range]
freeToOutParts = traverse freeToOutPart

-- | Refines to Range output part.
freeToOutPart :: Free.Part Range -> Free.RefineResult (Out.Part Range)
freeToOutPart (Free.PartPrim prim) = Success $ Out.PartPrim prim
freeToOutPart (Free.PartRecord record)
  | isFreeInjApp record = Out.PartInjApp <$> freeToInjApp record
  | otherwise = Out.PartRecord <$> freeToOutRecord record
freeToOutPart (Free.PartPropPath path) = Success $ Out.PartPropPath path
freeToOutPart (Free.PartShortcut shortcut)
  = freeShortcutToOutPart shortcut

isFreeInjApp :: Free.Record Range -> Bool
isFreeInjApp = isInjSymbol . Free.head

freeToInjApp :: Free.Record Range -> Free.RefineResult (Out.InjApp Range)
freeToInjApp record
    = Out.InjApp (Free.recordAnn record) (forceToInjSymbol $ Free.head record)
  <$> freeToInjParams (Free.properties record)

freeToInjParams :: [Free.Property Range] -> Free.RefineResult [Out.InjParam Range]
freeToInjParams = freeToInjParamsFromIdx 0

freeToInjParamsFromIdx :: Int -> [Free.Property Range] -> Free.RefineResult [Out.InjParam Range]
freeToInjParamsFromIdx _ [] = Success []
freeToInjParamsFromIdx n (x : xs)
  = (:) <$> freeToInjParamAtIdx n x <*> freeToInjParamsFromIdx (n + 1) xs

freeToInjParamAtIdx :: Int -> Free.Property Range -> Free.RefineResult (Out.InjParam Range)
freeToInjParamAtIdx _ (Free.PropertySingle val)
  = Out.InjParam ann <$> freeToOutput val
  where ann = getAnn val
freeToInjParamAtIdx n (Free.PropertyDef ann key val)
  | key /@= propKey
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "\"" ++ summary propKey ++ "\""
  , Free.actual = "\"" ++ summary key ++ "\""
  , Free.actualPr = pprintStr key
  }
  | otherwise = Out.InjParam ann <$> freeToOutput val
  where propKey = Out.idxPropKeys !! n

-- | Refines to Range output record.
freeToOutRecord :: Free.Record Range -> Free.RefineResult (Out.Record Range)
freeToOutRecord record
    = Record (Free.recordAnn record) (Free.head record)
  <$> freeToOutProperties (Free.properties record)

-- | Refines to output properties.
freeToOutProperties :: [Free.Property Range] -> Free.RefineResult [Out.Property Range]
freeToOutProperties = traverse freeToOutProperty

-- | Refines to Range output property.
freeToOutProperty :: Free.Property Range -> Free.RefineResult (Out.Property Range)
freeToOutProperty (Free.PropertySingle val)
  = Property ann Nothing <$> freeToOutput val
  where ann = getAnn val
freeToOutProperty (Free.PropertyDef ann key val)
  = Property ann (Just key) <$> freeToOutput val

-- | Refines a shortcut to an output part.
freeShortcutToOutPart :: Free.Shortcut Range -> Free.RefineResult (Out.Part Range)
freeShortcutToOutPart (Free.ShortList ann items)
  = Out.PartRecord <$> freeToOutRecord (Free.expandShortList ann items)

-- | Refines to a record type.
freeToRecordType :: Free.Value Range -> Free.RefineResult (RecordType Range)
freeToRecordType (Free.Value _ [Free.PartRecord record]) = freeRecordToRecordType record
freeToRecordType x@(Free.Value ann _)
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "record"
  , Free.actual = describeValueAsPart x
  , Free.actualPr = pprintStr x
  }

-- | Refines a record to a record type.
freeRecordToRecordType :: Free.Record Range -> Free.RefineResult (RecordType Range)
freeRecordToRecordType record
    = RecordType (Free.recordAnn record) (Free.head record)
  <$> freeToRecordTypeProperties (Free.properties record)

-- | Refines to record type "properties" (property declarations).
freeToRecordTypeProperties :: [Free.Property Range] -> Free.RefineResult [Symbol Range]
freeToRecordTypeProperties = traverse freeToRecordTypeProperty

-- | Refines to a record type "property" (property declaration).
freeToRecordTypeProperty :: Free.Property Range -> Free.RefineResult (Symbol Range)
freeToRecordTypeProperty (Free.PropertySingle x)
  = case Free.valueToPropKey x of
    Nothing
      -> Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
       { Free.expected = "property declaration"
       , Free.actual = "value"
       , Free.actualPr = pprintStr x
       }
    Just key -> Success key
  where ann = getAnn x
freeToRecordTypeProperty x@(Free.PropertyDef ann _ _)
  = Failure $ Free.entireRefineDiff ann Free.LocalRefineDiff
  { Free.expected = "property declaration"
  , Free.actual = "property definition"
  , Free.actualPr = pprintStr x
  }

describeValueAsPart :: Free.Value Range -> String
describeValueAsPart (Free.Value _ [Free.PartPrim _]) = "primitive"
describeValueAsPart (Free.Value _ [Free.PartRecord _]) = "record"
describeValueAsPart (Free.Value _ [Free.PartPropPath _]) = "path"
describeValueAsPart (Free.Value _ [Free.PartShortcut _]) = "shortcut"
describeValueAsPart (Free.Value _ []) = "empty"
describeValueAsPart (Free.Value _ _) = "union"
