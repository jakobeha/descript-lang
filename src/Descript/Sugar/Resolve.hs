module Descript.Sugar.Resolve
  ( resolve
  ) where

import Descript.Sugar.Data
import qualified Descript.BasicInj as BasicInj
import Descript.Misc

-- | Resolves dependencies using the given resolver.
resolve :: (Monad u)
        => BasicInj.DepResolver u
        -> Source an
        -> u (BasicInj.DirtyDepd Source an)
resolve rsvr src = (`Depd` src) <$> extra
  where extra = runDirtyT $ BasicInj.extraModule rsvr $ sourceImportCtx src
