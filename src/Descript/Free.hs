-- | Free grammar - can represent a lot of expressions, including
-- badly-formed ones. Expressions are "free" because they're later
-- refined to more restrictive types - for example, "free" values can be
-- refined to input values, output values, regular values, or record types.
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
