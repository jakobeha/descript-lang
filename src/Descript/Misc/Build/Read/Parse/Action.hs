module Descript.Misc.Build.Read.Parse.Action
  ( ParseAction
  , runLaterParser
  , runFirstParser
  ) where

import Descript.Misc.Build.Read.Parse.Loc
import Descript.Misc.Build.Read.Parse.Error
import Descript.Misc.Build.Read.File
import Descript.Misc.Loc
import Descript.Misc.Ann
import Descript.Misc.Error
import Text.Megaparsec
import Core.Text.Megaparsec
import Data.Text (Text)

-- | Intermedaite parse action. Parses an @o@, given the previously
-- built item (@i@) and the original file. Encodes failures in a
-- 'ParseError'.
type ParseAction i o = SFile -> i -> ParseResult o

-- | Parses a series of smaller tokens into a series of bigger tokens.
-- If the parser fails, converts the failure information back into
-- text using the given source so it can be displayed to the user.
-- Stores errors in a 'ParseResult'. Expects the parser to consume all
-- input.
runLaterParser :: (Ann i, Ord (i Range))
               => Parsec RangedError (RangeStream i) o
               -> ParseAction (RangeStream i) o
runLaterParser parser (SFile name contents) source
  = eitherToResult $ runParserFR parser name contents source

-- | Parses text into a series of tokens. Stores errors in a
-- 'ParseResult'. Expects the parser to consume all input.
runFirstParser :: Parsec RangedError Text o -> SFile -> ParseResult o
runFirstParser parser (SFile name contents)
  = eitherToResult $ runParserF parser name contents
