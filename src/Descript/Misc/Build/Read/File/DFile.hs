module Descript.Misc.Build.Read.File.DFile
  ( GenDFile (..)
  , fileName
  , fileContents
  ) where

import Descript.Misc.Build.Read.File.DepResolve
import Descript.Misc.Build.Read.File.SFile
import Data.Text

-- | A (dependent) source file. It can be an actual file on disk, or a
-- virtual representation. It just needs a name, contents, and a
-- resolver to other files. Dependencies are obtained through the monad
-- 'u', and they're of type 'a'.
data GenDFile u d
  = DFile
  { depResolver :: GenDepResolver u d d
  , sfile :: SFile
  } deriving (Show)

-- | The name of the file.
fileName :: GenDFile u d -> String
fileName = sfileName . sfile

-- | The textual representation of the file.
fileContents :: GenDFile u d -> Text
fileContents = sfileContents . sfile
