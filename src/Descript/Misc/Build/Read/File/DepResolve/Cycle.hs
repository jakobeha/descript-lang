module Descript.Misc.Build.Read.File.DepResolve.Cycle
  ( guardCycleResolver
  , guardPromoteCycleT
  ) where

import Descript.Misc.Build.Read.File.DepResolve.DepResolve
import Descript.Misc.Build.Read.File.DepError
import Descript.Misc.Build.Read.File.Scope
import Descript.Misc.Error
import Core.Control.Monad.Trans

-- | Fails if the dependencies try to resolve in a cycle - e.g.
-- dependency @A@ requires @B@, which requires @C@, which requires @A@.
-- To guard @unprotectedResolver@:
--
-- > let resolver = guardCycleResolver *> unprotectedResolver
guardCycleResolver :: GenDepResolver IO a b -> GenDepResolver IO a b
guardCycleResolver resolver
  = DepResolver
  { showDepResolver = "guardCycleResolver " ++ show resolver
  , anchorResolver = guardCycleAbsResolver . anchorResolver resolver
  }

-- | Fails if the dependencies try to resolve in a cycle - e.g.
-- dependency @A@ requires @B@, which requires @C@, which requires @A@.
-- To guard @unprotectedResolver@:
--
-- > let resolver = guardCycleResolver *> unprotectedResolver
guardCycleAbsResolver :: GenAbsDepResolver IO a b -> GenAbsDepResolver IO a b
guardCycleAbsResolver = guardCycleAbsResolver' []

-- | Fails if the dependencies try to resolve in a cycle - e.g.
-- dependency @A@ requires @B@, which requires @C@, which requires @A@.
-- Assumes the given dependencies are using this resolver - if they
-- cycle, will fail.
guardCycleAbsResolver' :: [AbsScope]
                       -> GenAbsDepResolver IO a b
                       -> GenAbsDepResolver IO a b
guardCycleAbsResolver' dpds = AbsDepResolver . resolveGuardCycle dpds

resolveGuardCycle :: [AbsScope]
                  -> GenAbsDepResolver IO a b
                  -> GenDepResolverArgs IO a b
                  -> GenDepResultT IO a
resolveGuardCycle dpds resolver (DepResolverArgs srsvr finalize vscope ascope) = do
  guardCycleT ascope dpds
  let newDpds = ascope : dpds
      newSrsvr = guardCycleAbsResolver' newDpds srsvr
      newArgs = DepResolverArgs newSrsvr finalize vscope ascope
  resolveAbsDepRec resolver newArgs

-- | Fails with 'DepCycles' if adding the given dependency to the given
-- chain of dependencies creates a cycle (if the chain contains the
-- dependency).
guardCycle :: AbsScope -> [AbsScope] -> GenDepResult ()
guardCycle new dpds
  | new `elem` dpds = Failure $ DepCycles [new]
  | otherwise = Success ()

-- | Hoisted 'guardCycle' for convenience.
guardCycleT :: (Monad u) => AbsScope -> [AbsScope] -> GenDepResultT u ()
guardCycleT new = hoist . guardCycle new

-- | Fails if one of the dependency "errors" is a 'DepCycles'. Typically
-- because data with other dependency errors is still interpreted for
-- more errors, but 'DepCycles' errors generate redundant messages
-- later, so when they're encountered, resolution immediately fails.
guardPromoteCycle :: AbsScope -> [TagdDepError an] -> GenDepResult ()
guardPromoteCycle _ [] = Success ()
guardPromoteCycle scope (x : xs)
  = guardPromoteCycle1 scope x >> guardPromoteCycle scope xs

guardPromoteCycle1 :: AbsScope -> TagdDepError an -> GenDepResult ()
guardPromoteCycle1 scope terr
  = case tagdDepError terr of
         DepCycles inters -> Failure $ DepCycles $ scope : inters
         _ -> Success ()

-- | Hoisted 'guardPromoteCycle' for convenience.
guardPromoteCycleT :: (Monad u) => AbsScope -> [TagdDepError an] -> GenDepResultT u ()
guardPromoteCycleT scope = hoist . guardPromoteCycle scope
