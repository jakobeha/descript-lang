module Descript.Misc.Build.Read.File.DepResolve.File
  ( defaultSFileResolver
  ) where

import Descript.Misc.Build.Read.File.DepResolve.DepResolve
import Descript.Misc.Build.Read.File.DepError
import Descript.Misc.Build.Read.File.Scope
import Descript.Misc.Build.Read.File.SFile
import Descript.Misc.Error
import Data.Maybe
import qualified Data.Text.IO as Text
import Control.Applicative
import Control.Exception
import System.IO.Error
import System.FilePath
import System.Directory
import Paths_descript_lang

-- | Resolves dependencies in the filesystem within the given folder,
-- or within the user's dependency folder.
-- The dependencies are resolved to files with raw text - typically you
-- would bind another resolver to process them into source.
defaultSFileResolver :: (Monoid a) => FilePath -> GenDepResolver IO a [SFile]
defaultSFileResolver basePath
  = localSFileResolver basePath <|> sharedGenDepSFileResolver

-- | Resolves dependencies in the filesystem within the given folder.
-- The dependencies are resolved to files with raw text - typically you
-- would bind another resolver to process them into source.
localSFileResolver :: (Monoid a) => FilePath -> GenDepResolver IO a [SFile]
localSFileResolver basePath
  = DepResolver
  { showDepResolver = "localSFileResolver " ++ show basePath
  , anchorResolver = AbsDepResolver . resolveLocalT basePath . Just
  }

-- | Resolves shared dependencies -- installed dependencies which are
-- accessible to all modules with the same path, like @Base@.
-- The dependencies are resolved to files with raw text - typically you
-- would bind another resolver to process them into source.
sharedGenDepSFileResolver :: (Monoid a) => GenDepResolver IO a [SFile]
sharedGenDepSFileResolver
  = DepResolver
  { showDepResolver = "sharedGenDepSFileResolver"
  , anchorResolver
      = const
      $ AbsDepResolver
      $ resolveLocalT' getSharedDepPath Nothing
  }

resolveLocalT :: (Monoid a)
              => FilePath
              -> Maybe AbsScope
              -> GenDepResolverArgs IO a [SFile]
              -> GenDepResultT IO a
resolveLocalT basePath anchor = ResultT . resolveLocal basePath anchor

resolveLocalT' :: (Monoid a)
               => IO FilePath
               -> Maybe AbsScope
               -> GenDepResolverArgs IO a [SFile]
               -> GenDepResultT IO a
resolveLocalT' getBasePath anchor
  = ResultT . resolveLocal' getBasePath anchor

resolveLocal :: (Monoid a)
             => FilePath
             -> Maybe AbsScope
             -> GenDepResolverArgs IO a [SFile]
             -> IO (GenDepResult a)
resolveLocal basePath anchor args
  = handle (failResolveLocal dirPath)
  $ forceResolveLocal basePath anchor args
  where dirPath = scopeDirPath basePath relScope
        relScope = depArgsRealScope args `scopeRelToSib'` anchor

resolveLocal' :: (Monoid a)
              => IO FilePath
              -> Maybe AbsScope
              -> GenDepResolverArgs IO a [SFile]
              -> IO (GenDepResult a)
resolveLocal' getBasePath anchor args = continue =<< getBasePath
  where continue basePath = resolveLocal basePath anchor args

forceResolveLocal :: (Monoid a)
                  => FilePath
                  -> Maybe AbsScope
                  -> GenDepResolverArgs IO a [SFile]
                  -> IO (GenDepResult a)
forceResolveLocal basePath anchor args = do
  let relScope = depArgsRealScope args `scopeRelToSib'` anchor
      filePath' = scopeFilePath basePath relScope
      dirPath = scopeDirPath basePath relScope
  isFile <- doesFileExist filePath'
  isDir <- doesDirectoryExist dirPath
  case (isFile, isDir) of
    (True, _) -> forceResolveLocalFile filePath' args
    (False, True) -> forceResolveLocalDir dirPath args
    (False, False) -> ioError $ errDoesNotExistInResolve dirPath

forceResolveLocalFile :: (Monoid a)
                      => FilePath
                      -> GenDepResolverArgs IO a [SFile]
                      -> IO (GenDepResult a)
forceResolveLocalFile path args
    = runResultT $ depArgsFinalize args
  =<< ResultT (forceResolveLocalFile' path)

forceResolveLocalFile' :: FilePath -> IO (GenDepResult [SFile])
forceResolveLocalFile' path
  = Success . pure . mkSFile path <$> Text.readFile path

forceResolveLocalDir :: (Monoid a)
                     => FilePath
                     -> GenDepResolverArgs IO a [SFile]
                     -> IO (GenDepResult a)
forceResolveLocalDir path (DepResolverArgs srsvr _ vscope ascope)
    = runResultT
    . foldMap (resolveAbsDep' srsvr vscope . snocAbsPath ascope)
    . mapMaybe filenameToModuleName
  =<< listDirectory path

errDoesNotExistInResolve :: FilePath -> IOError
errDoesNotExistInResolve path
  = mkIOError doesNotExistErrorType msg Nothing $ Just path
  where msg = "no dependency here (not a file or directory)"

failResolveLocal :: FilePath -> IOError -> IO (GenDepResult a)
failResolveLocal path = pure . Failure . ioErrorToDepError path

ioErrorToDepError :: FilePath -> IOError -> AnonDepError
ioErrorToDepError path err
  | isDoesNotExistError err = DepNotExist [path ++ "(.dscr)"]
  | otherwise = DepNotReadable

filenameToModuleName :: FilePath -> Maybe FilePath
filenameToModuleName path
  = case ext of
         "" -> Just base
         ".dscr" -> Just base
         _ -> Nothing
  where (base, ext) = splitExtension path

-- | This directory contains modules which can be used by a Descript
-- file anywhere.
getSharedDepPath :: IO FilePath
getSharedDepPath = getDataFileName "resources/modules"
