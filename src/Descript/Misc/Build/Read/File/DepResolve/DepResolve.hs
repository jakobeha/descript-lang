module Descript.Misc.Build.Read.File.DepResolve.DepResolve
  ( GenDepResolver (..)
  , GenAbsDepResolver (..)
  , GenDepResolverArgs (..)
  , resolveDep
  , resolveDepRec
  , resolveAbsDep
  , resolveAbsDep'
  , anchoredResolver
  ) where

import Descript.Misc.Build.Read.File.DepError
import Descript.Misc.Build.Read.File.Scope
import Descript.Misc.Error
import Core.Data.Functor
import Control.Applicative
import Core.Control.Applicative

-- | Finds the dependency at the given module, relative to another given
-- module.
data GenDepResolver u a b
  = DepResolver
  { showDepResolver :: String -- ^ Used for 'Show'.
  , anchorResolver :: AbsScope -> GenAbsDepResolver u a b
  }

-- | Finds the dependency at the given module.
-- Resolves recursive dependencies using the given resolver - this
-- allows resolvers to be composed, and when the resolver is actually
-- used, it will get itself passed.
newtype GenAbsDepResolver u a b
  = AbsDepResolver
  { resolveAbsDepRec :: GenDepResolverArgs u a b
                     -> GenDepResultT u a
  }

-- | Arguents passed to a dependency resolver.
data GenDepResolverArgs u a b
  = DepResolverArgs
  { -- | Resolves recursive dependencies.
    depArgsRecResolver :: GenAbsDepResolver u a a
    -- | Finishes resolving. Applied to the resolver's dependency.
    -- Needed because this resolver might be part of a chain, and this
    -- allows sub-resolvers to be the type of the chain, not just the
    -- type of this resolver.
  , depArgsFinalize :: b -> GenDepResultT u a
    -- | The scope of the dependency to the dependent. Should be
    -- substituted with the real scope, so when the dependent references
    -- this scope it gets the real scope.
  , depArgsVirtScope :: AbsScope
    -- | The actual scope of the dependency. The "main" argument.
  , depArgsRealScope :: AbsScope
  }

instance Show (GenDepResolver u a b) where
  show = showDepResolver

instance (Functor u) => Functor (GenDepResolver u a) where
  fmap f x
    = DepResolver
    { showDepResolver = "f <$> " ++ showDepResolver x
    , anchorResolver = f <<$>> anchorResolver x
    }

instance (Functor u) => Functor (GenAbsDepResolver u a) where
  fmap f x = AbsDepResolver resolve
    where resolve args
            = resolveAbsDepRec x args
            { depArgsFinalize = depArgsFinalize args . f
            }

instance (Functor u) => Applicative (GenDepResolver u a) where
  pure x
    = DepResolver
    { showDepResolver = "pure x"
    , anchorResolver = pure2 x
    }
  f <*> x
    = DepResolver
    { showDepResolver = showDepResolver f ++ " <*> " ++ showDepResolver x
    , anchorResolver = anchorResolver f <<*>> anchorResolver x
    }

instance (Functor u) => Applicative (GenAbsDepResolver u a) where
  pure x = AbsDepResolver resolve
    where resolve args = depArgsFinalize args x
  fx <*> xx = AbsDepResolver resolveF
    where resolveF args
            = resolveAbsDepRec fx args
            { depArgsFinalize = resolveX args
            }
          resolveX args f
            = resolveAbsDepRec xx args
            { depArgsFinalize = depArgsFinalize args . f
            }

instance (Functor u) => Monad (GenDepResolver u a) where
  return = pure
  x >>= f
    = DepResolver
    { showDepResolver = showDepResolver x ++ " >>= f"
    , anchorResolver = AbsDepResolver . resolveF
    }
    where resolveF anchor args
            = resolveDepRec x anchor args
            { depArgsFinalize = resolveX anchor args . f }
          resolveX anchor args inter = resolveDepRec inter anchor args

instance (Functor u) => Monad (GenAbsDepResolver u a) where
  return = pure
  fx >>= f = AbsDepResolver resolveF
    where resolveF args
            = resolveAbsDepRec fx args
            { depArgsFinalize = resolveX args . f
            }
          resolveX args xx = resolveAbsDepRec xx args

-- | Will use the second resolver if the first resolver can't find the
-- module. If the first resolver gets another resolver or succeeds,
-- will return its result.
instance (Monad u) => Alternative (GenDepResolver u a) where
  empty
    = DepResolver
    { showDepResolver = "empty"
    , anchorResolver = const empty
    }
  x <|> y
    = DepResolver
    { showDepResolver = showDepResolver x ++ " <|> " ++ showDepResolver y
    , anchorResolver = (<|>) <$> anchorResolver x <*> anchorResolver y
    }

-- | Will use the second resolver if the first resolver can't find the
-- module. If the first resolver gets another resolver or succeeds,
-- will return its result.
instance (Monad u) => Alternative (GenAbsDepResolver u a) where
  empty
    = AbsDepResolver resolve
    where resolve _ = mkFailureT $ DepNotExist []
  fx <|> fy
    = AbsDepResolver resolve
    where resolve args = ResultT $ do
            xres <- runResultT $ resolveAbsDepRec fx args
            case xres of
              Failure (DepNotExist xPaths) -> do
                yres <- runResultT $ resolveAbsDepRec fy args
                case yres of
                  Failure (DepNotExist yPaths)
                    -> pure $ Failure $ DepNotExist paths
                    where paths = xPaths ++ yPaths
                  _ -> pure yres
              _ -> pure xres

-- | Resolves the dependency and its dependencies using the given resolver.
resolveDep :: (Applicative u)
           => GenDepResolver u a a
           -> AbsScope
           -> AbsScope
           -> GenDepResultT u a
resolveDep rsvr = resolveAbsDep . anchorResolver rsvr

-- | Resolves the dependency and its dependencies using the given
-- resolver, resolving recursive dependencies with the resolver given in
-- the arguments.
resolveDepRec :: GenDepResolver u a b
              -> AbsScope
              -> GenDepResolverArgs u a b
              -> GenDepResultT u a
resolveDepRec rsvr = resolveAbsDepRec . anchorResolver rsvr

-- | Resolves the dependency and its dependencies using the given resolver.
resolveAbsDep :: (Applicative u)
              => GenAbsDepResolver u a a
              -> AbsScope
              -> GenDepResultT u a
resolveAbsDep rsvr scope = resolveAbsDep' rsvr scope scope

-- | Resolves the dependency and its dependencies using the given
-- resolver. Allows the dependent to use a different scope than actual.
resolveAbsDep' :: (Applicative u)
               => GenAbsDepResolver u a a
               -> AbsScope
               -> AbsScope
               -> GenDepResultT u a
resolveAbsDep' rsvr vscope
  = resolveAbsDepRec rsvr . DepResolverArgs rsvr pure vscope

-- | A 'DepResolver' which discards its given anchor, and uses the one
-- already used by the 'AbsDepResolver'.
anchoredResolver :: GenAbsDepResolver u a b -> GenDepResolver u a b
anchoredResolver x
  = DepResolver
  { showDepResolver = "anchoredResolver x"
  , anchorResolver = const x
  }
