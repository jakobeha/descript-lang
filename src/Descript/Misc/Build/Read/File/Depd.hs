{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}

module Descript.Misc.Build.Read.File.Depd
  ( GenDepd (..)
  , mapDep
  ) where

-- | An AST and its dependencies.
data GenDepd d a
  = Depd
  { depdDep :: d
  , depdVal :: a
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | Transforms the dependency.
mapDep :: (d1 -> d2) -> GenDepd d1 a -> GenDepd d2 a
mapDep f (Depd dep x) = Depd (f dep) x
