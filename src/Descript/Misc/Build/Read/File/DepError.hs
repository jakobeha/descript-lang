{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}

module Descript.Misc.Build.Read.File.DepError
  ( AnonDepError (..)
  , TagdDepError (..)
  , GenDepResult
  , GenDepResultT
  , GenDirtyDep
  , GenDirtyDepT
  , mapDirtyDepdAnn
  ) where

import Descript.Misc.Build.Read.File.Depd
import Descript.Misc.Build.Read.File.Scope
import Descript.Misc.Error
import Descript.Misc.Ann
import Descript.Misc.Summary
import Core.Data.String

-- | An error encountered trying to load a dependency. Doesn't specify
-- the dependency's scope (must be inferred).
data AnonDepError
  = DepNotExist [String]
  | DepNotReadable
  | DepNotBuild String
  | DepCycles [AbsScope]
  deriving (Eq, Ord, Read, Show)

-- | An error encountered trying to load a dependency. Specifies the
-- dependency's scope.
data TagdDepError an
  = TagdDepError
  { tagdDepErrorAnn :: an
  , tagdDepErrorScope :: AbsScope
  , tagdDepError :: AnonDepError
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | The result of trying to load a dependency with no fallback.
type GenDepResult a = Result AnonDepError a

-- | The result of trying to load a dependency with no fallback (stacked).
type GenDepResultT u a = ResultT AnonDepError u a

-- | The result of trying to load dependencies with fallback.
type GenDirtyDep an a = Dirty (TagdDepError an) a

-- | The result of trying to load dependencies with fallback (stacked).
type GenDirtyDepT an u a = DirtyT (TagdDepError an) u a

instance Ann TagdDepError where
  getAnn = tagdDepErrorAnn

instance SummaryWithAnn TagdDepError where
  baseSummary (TagdDepError _ depScope err)
    = anonDepErrorSummary suffix err
    where suffix = " - " ++ summary depScope

instance (AnnSummary an) => Summary (TagdDepError an) where
  summary = summaryWithAnn

instance Summary AnonDepError where
  summary = anonDepErrorSummary ""

-- | Transforms the annotations within the @DirtyDepd@. Annotations are
-- in the value and errors associated with the dependency, but \not\ the
-- dependency itself.
mapDirtyDepdAnn :: (Functor a)
                => (an1 -> an2)
                -> GenDepd (GenDirtyDep an1 d) (a an1)
                -> GenDepd (GenDirtyDep an2 d) (a an2)
mapDirtyDepdAnn f (Depd ddep x)
  = Depd (mapWarnings (fmap f) ddep) (f <$> x)

anonDepErrorSummary :: String
                    -> AnonDepError
                    -> String
anonDepErrorSummary sufx (DepNotExist paths)
  = unlines
  ( ("couldn't find module" ++ sufx ++ ", tried:")
  : map indentBullet paths )
anonDepErrorSummary sufx DepNotReadable = "couldn't read module" ++ sufx
anonDepErrorSummary sufx (DepNotBuild msg)
  = "couldn't build module" ++ sufx ++ ":\n" ++ msg
anonDepErrorSummary _ (DepCycles relInters)
  = unlines
  ( "module depends on this module, forming a cycle:"
  : map (indentBullet . summary) relInters )
