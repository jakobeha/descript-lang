-- | Contains terms for nodes in all phases.
--
-- > import qualified ...Term as Term
module Descript.Misc.Build.Process.Validate.Term
  ( Term (..)
  ) where

import Descript.Misc.Summary
import Core.Data.List
import Data.Char

-- | A type of expression. This enum covers a broad range of different expressions.
data Term
  = Import
  | RecordDecl
  | Input
  | Output
  | Property
  | Query
  deriving (Eq, Ord, Read, Show)

instance Summary Term where
  summary RecordDecl = "record declaration"
  summary x = overHead toLower $ show x -- Covers most cases
