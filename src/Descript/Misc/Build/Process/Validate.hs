-- | Checks that Descript source is well-formed before it's interpreted.
-- If the source isn't well-formed, generates user-friendly problems.
module Descript.Misc.Build.Process.Validate
  ( module Descript.Misc.Build.Process.Validate.Problem
  , Term
  ) where

import Descript.Misc.Build.Process.Validate.Problem
import Descript.Misc.Build.Process.Validate.Term (Term)
