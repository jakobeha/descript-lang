module Descript.Misc.Build.Write.Compile.Result
  ( CompileError (..)
  , CompileResult
  ) where

import Descript.Misc.Build.Write.Compile.Package
import Descript.Misc.Error
import Descript.Misc.Summary

-- | An error created during compilation
newtype CompileError = NotFinal String
  deriving (Eq, Ord, Read, Show)

type CompileResult = Result CompileError Package

instance Summary CompileError where
  summary (NotFinal valPr) = "not final (bad type): " ++ valPr
