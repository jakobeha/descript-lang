{-# LANGUAGE TemplateHaskell #-}

module Descript.Misc.Build.Write.Compile.Code
  ( languageExt
  ) where

import Data.Char
import Data.Maybe
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Yaml.TH
import Instances.TH.Lift ()

-- | All the known source code file extensions - maps from the language
-- to the extension.
knownExts :: Map String String
knownExts = $$(decodeFile "resources/extensions.yaml")

-- | Gets the file extension for source code of the given language.
languageExt :: String -> String
languageExt = languageExt' . map toLower

-- | Gets the file extension for source code of the given language
-- (in lowercase).
languageExt' :: String -> String
languageExt' x = x `fromMaybe` (knownExts Map.!? x)
