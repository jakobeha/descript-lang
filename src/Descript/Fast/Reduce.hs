-- | Reudction algorithm.
module Descript.Fast.Reduce
  ( reduce
  , reduceOnce ) where

import Descript.BasicInj.Data.Reducer
import Descript.BasicInj.Data.Value.Reg
import Descript.Misc.Error

-- | A context's reducers organized so they can quickly be found when needed.
data FastReduceCtx

-- | Organizes the reducers to they can be found quicker when reducing a value.
optimizeReduceCtx :: ReduceCtx () -> FastReduceCtx
optimizeReduceCtx = undefined

-----

-- | Applies the context's reducers to the value, until they can't
-- be applied anymore.
reduce :: ReduceCtx () -> Value () -> Value ()
reduce ctx value = reduce' (optimizeReduceCtx ctx) value

-- | Applies the context's reducers to the value, until they can't
-- be applied anymore.
reduce' :: FastReduceCtx -> Value () -> Value ()
reduce' ctx value
  = case reduceOnce ctx value of
         Success next -> reduce' ctx next
         Failure () -> value

-- | Applies the context's reducers to the value once, returning a new
-- value if it reduced, or a failure if it couldn't be reduced at all.
reduceOnce :: FastReduceCtx -> Value () -> UResult (Value ())
reduceOnce = undefined
