module Descript.Build.Cache.DepCache
  ( cacheResolver
  ) where

import Descript.Build.Read
import Descript.Misc
import qualified Data.HashTable.IO as HashTable
import Control.Applicative
import Control.Monad.Trans.Class
import Control.Monad.IO.Class

type HashTable k v = HashTable.BasicHashTable k v

-- | Caches dependencies in a workspace (collection of related modules)
-- so they each need to be loaded once.
newtype DepCache = DepCache (HashTable AbsScope Dep) deriving (Show)

-- | Caches the resolved modules.
cacheResolver :: (MonadIO u) => DepResolver u -> u (DepResolver u)
cacheResolver resolver = cacheResolver' resolver <$> newCache

-- | Caches the resolved modules with the given cache.
cacheResolver' :: (MonadIO u) => DepResolver u -> DepCache -> DepResolver u
cacheResolver' resolver cache
    = loadCacheResolver cache
  <|> saveCacheResolver cache resolver

-- | Resolves by loading from the class.
loadCacheResolver :: (MonadIO u) => DepCache -> DepResolver u
loadCacheResolver cache
  = DepResolver
  { showDepResolver = "loadDepResolver " ++ show cache
  , anchorResolver = const $ AbsDepResolver $ resolveByLoadCache cache
  }

-- | Resolves using the underlying resolver, then saves the result to
-- the cache.
saveCacheResolver :: (MonadIO u) => DepCache -> DepResolver u -> DepResolver u
saveCacheResolver cache resolver = saveCacheResolver' cache =<< resolver

-- Saves the result to the cache.
saveCacheResolver' :: (MonadIO u) => DepCache -> Dep -> DepResolver u
saveCacheResolver' cache dep
  = DepResolver
  { showDepResolver = "saveCacheResolver " ++ show cache ++ " " ++ show dep
  , anchorResolver = const $ AbsDepResolver $ resolveBySaveCache cache dep
  }

resolveByLoadCache :: (MonadIO u) => DepCache -> DepResolverArgs u -> DepResultT u
resolveByLoadCache cache (DepResolverArgs _ finalize vscope scope)
  | vscope /= scope = mkFailureT $ DepNotExist [] -- Needed to prevent conflicts
  | otherwise = finalize =<< resolveByLoadCache' cache scope

resolveByLoadCache' :: (MonadIO u) => DepCache -> AbsScope -> DepResultT u
resolveByLoadCache' cache scope = do
  depOpt <- lift $ loadCache cache scope
  case depOpt of
    Nothing -> mkFailureT $ DepNotExist [] -- Doesn't mention resolver tried caches
    Just dep -> mkSuccessT dep

resolveBySaveCache :: (MonadIO u) => DepCache -> Dep -> DepResolverArgs u -> DepResultT u
resolveBySaveCache cache dep (DepResolverArgs _ finalize _ scope)
  = finalize =<< resolveBySaveCache' cache dep scope

resolveBySaveCache' :: (MonadIO u) => DepCache -> Dep -> AbsScope -> DepResultT u
resolveBySaveCache' cache dep scope = dep <$ lift (saveCache cache scope dep)

newCache :: (MonadIO u) => u DepCache
newCache = liftIO $ DepCache <$> HashTable.new

loadCache :: (MonadIO u) => DepCache -> AbsScope -> u (Maybe Dep)
loadCache (DepCache ctable) = liftIO . HashTable.lookup ctable

saveCache :: (MonadIO u) => DepCache -> AbsScope -> Dep -> u ()
saveCache (DepCache ctable) scope = liftIO . HashTable.insert ctable scope
