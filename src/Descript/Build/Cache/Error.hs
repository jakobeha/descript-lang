module Descript.Build.Cache.Error
  ( CacheError (..)
  , CacheWarning (..)
  ) where

import Descript.Build.Cache.FileCache
import Descript.Misc

-- | An error obtained updating a cache. Prevents parts of the cache
-- from being updated.
--
-- Note: 'CacheFileNotFound', 'CachePatchBeforeText', and
-- 'CachePatchBeforeText' are internal errors caused by a bad request.
-- The others are user errors which could appear even when the IDE is
-- working properly.
data CacheError
  = CacheFileNotFound
  | CachePatchBeforeText
  | CacheRefactorBeforeParse
  | CacheParseError FileCache (ParseError Char)
  | CacheRefactorError FileCache RefactorError
  deriving (Eq, Read, Show)

-- | A warning obtained updating a cache. Allows the delegate to cancel
-- the update, but also allows it to continue.
data CacheWarning
  = CacheRefactorWarning FileCache [RefactorWarning]
  deriving (Eq, Ord, Read, Show)
