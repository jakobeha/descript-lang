-- | Keeps track of intermediate ASTs and applies updates. Unlike simple
-- actions, cached actions can be faster and use previous state to
-- understand "bad" ASTs.
-- Useful for complicated, changing processes like language servers.
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
