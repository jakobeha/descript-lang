{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- | Organizes build results.
module Descript.Build.Error
  ( BuildError (..)
  , BuildResult
  , BuildResultT
  ) where

import Descript.Misc
import Data.Semigroup

-- | Any type of build error.
data BuildError
  = BuildParseError (ParseError Char)
  | BuildExpectedProgramError
  | BuildValidateError [Problem SrcAnn]
  | BuildCompileError CompileError
  deriving (Eq, Read, Show)

-- | A final result from building (parsing, interpreting, and
-- outputting) a source. If the build succeeded, contains  the output.
-- If the build failed, contains the error.
type BuildResult a = Result BuildError a

-- | A stacked 'BuildResult'.
type BuildResultT u a = ResultT BuildError u a

-- | When combined, takes the earlier error.
instance Semigroup BuildError where
  BuildParseError x <> BuildParseError y = BuildParseError $ x <> y
  BuildParseError x <> _ = BuildParseError x
  BuildExpectedProgramError <> _ = BuildExpectedProgramError
  BuildValidateError xs <> BuildValidateError ys = BuildValidateError $ xs <> ys
  BuildValidateError xs <> _ = BuildValidateError xs
  BuildCompileError x <> _ = BuildCompileError x

instance FileSummary BuildError where
  summaryF file (BuildParseError err) = parseErrorSummary file err
  summaryF _ BuildExpectedProgramError = "expected a program, given a module"
  summaryF _ (BuildValidateError probs) = validateErrorSummary probs
  summaryF _ (BuildCompileError err) = "compile error: " ++ summary err
