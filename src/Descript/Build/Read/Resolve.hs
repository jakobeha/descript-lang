module Descript.Build.Read.Resolve
  ( Dep
  , DepResult
  , DepResultT
  , DirtyDep
  , DirtyDepT
  , Depd
  , DirtyDepd
  , DepResolver
  , DepResolverArgs
  , DFile
  ) where

import qualified Descript.BasicInj as BasicInj

-- Note: If there are multiple dependency requiring values, replace with
-- custom @Gen...@ instances which contain all those values as
-- dependencies. Create a wrapper for 'BasicInj.validate' which takes
-- all the dependencies dirty, passes the 'BasicInj' dependencies to
-- 'BasicInj.validate', and returns the (ok) result with all of the
-- dependencies but removes the dirty wrapper (probably reimplement
-- 'BasicInj.validate's control structure and call 'BasicInj.validate''
-- directly).

type Dep = BasicInj.Dep
type DepResult = BasicInj.DepResult
type DepResultT u = BasicInj.DepResultT u
type DirtyDep an = BasicInj.DirtyDep an
type DirtyDepT an u = BasicInj.DirtyDepT an u
type Depd a an = BasicInj.Depd a an
type DirtyDepd a an = BasicInj.DirtyDepd a an
type DepResolver u = BasicInj.DepResolver u
type DepResolverArgs u = BasicInj.DepResolverArgs u
type DFile u = BasicInj.DFile u
