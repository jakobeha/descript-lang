module Descript.Build.Read.File
  ( mkFile
  , loadFile
  , defaultResolver
  ) where

import Descript.Build.Read.Read
import Descript.Build.Read.Resolve
import qualified Descript.BasicInj as BasicInj
import Descript.Misc
import Data.Text (Text)
import qualified Data.Text.IO as Text
import Core.Control.Monad.Trans
import System.FilePath

-- | Creates a file with the given path and contents, which uses the
-- default resolver.
mkFile :: FilePath -> Text -> DFile IO
mkFile path contents
  = DFile
  { depResolver = defaultResolver $ takeDirectory path
  , sfile = mkSFile path contents
  }

-- | Reads the file with the given path.
loadFile :: FilePath -> IO (DFile IO)
loadFile path = mkFile path <$> Text.readFile path

defaultResolver :: FilePath -> DepResolver IO
defaultResolver path = readResolver =<< defaultSFileResolver path

readResolver :: [SFile] -> DepResolver IO
readResolver pfiles
  = DepResolver
  { showDepResolver = "readResolver " ++ show pfiles
  , anchorResolver = const $ AbsDepResolver resolve
  }
  where resolve args = foldMap (readDep args) pfiles

readDep :: DepResolverArgs IO -> SFile -> DepResultT IO
readDep (DepResolverArgs srsvr finalize vscope ascope) sfile'
  = finalizeR =<< validateR =<< readSrcR file
  where readSrcR = gresToDres (summaryF sfile') . readSrc
        validateR ddsrc = do
          guardPromoteCycleT ascope $ dirtyWarnings $ depdDep ddsrc
          gresToDres validateErrorSummary $ hoist $ BasicInj.validate ddsrc
        finalizeR
          = finalize
          . BasicInj.dsourceAModule
          . fmap (BasicInj.globalSubstScope vscope ascope)
        gresToDres summary' = mapErrorT $ DepNotBuild . summary'
        file = DFile (anchoredResolver srsvr) sfile'
