module Descript.Build.Read.Read
  ( readSrc
  , readInputValIn
  , readOutputValIn
  ) where

import Descript.Build.Read.Resolve
import Descript.Build.Read.Parse
import qualified Descript.BasicInj.Data.Value.In as BasicInj.In
import qualified Descript.BasicInj.Data.Value.Out as BasicInj.Out
import qualified Descript.BasicInj as BasicInj
import qualified Descript.Sugar as Sugar
import Descript.Misc
import Core.Control.Monad.Trans
import Control.Monad.Trans.Class

-- | Parses source, resolves its dependencies, and refines it.
readSrc :: (Monad u)
        => DFile u
        -> ParseResultT u (DirtyDepd BasicInj.Source SrcAnn)
readSrc (DFile rsvr sfile')
  = refineR =<< resolveR rsvr =<< parseR sfile'
  where refineR = pure . Sugar.refineDDepd
        resolveR rsvr' = lift . Sugar.resolve rsvr'
        parseR = hoist . parse

readInputValIn :: AbsScope
               -> BasicInj.RecordCtx ()
               -> SFile
               -> ParseResult (BasicInj.In.Value SrcAnn)
readInputValIn scope ctx
  = fmap (Sugar.refineInputValIn scope ctx) . parseInputVal

readOutputValIn :: AbsScope
                -> BasicInj.RecordCtx ()
                -> BasicInj.In.Value ()
                -> SFile
                -> ParseResult (BasicInj.Out.Value SrcAnn)
readOutputValIn scope ctx in'
  = fmap (Sugar.refineOutputValIn scope ctx in') . parseOutputVal
