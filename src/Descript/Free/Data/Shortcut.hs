{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedStrings #-}

module Descript.Free.Data.Shortcut
  ( GenShortcut (..)
  ) where

import Descript.Misc
import Data.Monoid

-- | Syntax sugar - expands into a value or part, for a value of the
-- given type.
data GenShortcut v an
  = -- | A list - @[foo, bar, baz] => Cons[foo, Cons[bar, Cons[baz, Nil[]]]]@.
    ShortList an [v an]
  deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

instance (Functor v, Foldable v, Traversable v) => Ann (GenShortcut v) where
  getAnn (ShortList ann _) = ann

instance (Printable v) => Printable (GenShortcut v) where
  aprintRec sub (ShortList _ items)
    = "$[" <> pintercal ", " (map sub items) <> "]"

instance (Printable v, Show an, Show (v an)) => Summary (GenShortcut v an) where
  summaryRec = pprintSummaryRec
