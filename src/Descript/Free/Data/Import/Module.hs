{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}

module Descript.Free.Data.Import.Module
  ( ModulePath (..)
  , PhaseUps (..)
  , ModuleDecl (..)
  , defModuleDecl
  , moduleDeclScope
  , modulePathScope
  ) where

import Descript.Free.Data.Atom
import Descript.Misc
import Data.Monoid
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NonEmpty

-- | A path to a module.
data ModulePath an
  = ModulePath
  { modulePathAnn :: an
  , modulePathElems :: NonEmpty (Symbol an)
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | Denotes that imported reducers should be shifted up a certain
-- amount of phases.
data PhaseUps an
  = PhaseUps
  { phaseUpsAnn :: an
  , phaseUpsNum :: Int
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | An module declaration.
data ModuleDecl an
  = ModuleDecl
  { moduleDeclAnn :: an
  , moduleDeclPath :: ModulePath an
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

instance Ann ModuleDecl where
  getAnn = moduleDeclAnn

instance Ann PhaseUps where
  getAnn = phaseUpsAnn

instance Ann ModulePath where
  getAnn = modulePathAnn

instance Printable ModuleDecl where
  aprintRec sub (ModuleDecl _ path) = pimp' $ "module " <> sub path
    where pimp' = pimpIf $ NonEmpty.length (modulePathElems path) == 1

instance Printable PhaseUps where
  -- Might need @plex@, probably not
#if MIN_VERSION_base(4,11,0)
  aprint (PhaseUps _ num) = stimes num "^"
#else
  aprint (PhaseUps _ num) = mconcat $ replicate num "^"
#endif

instance Printable ModulePath where
  aprintRec sub (ModulePath _ syms)
    = pintercal ">" $ map sub $ NonEmpty.toList syms

instance (Show an) => Summary (ModuleDecl an) where
  summaryRec = pprintSummaryRec

instance (Show an) => Summary (PhaseUps an) where
  summaryRec = pprintSummaryRec

instance (Show an) => Summary (ModulePath an) where
  summaryRec = pprintSummaryRec

-- | If a module declaration isn't provided in a source file, this one
-- is implicitly used. Contains one path element - the file's name.
defModuleDecl :: SFile -> ModuleDecl ()
defModuleDecl = ModuleDecl () . scopeModulePath . defaultFileScope

scopeModulePath :: AbsScope -> ModulePath ()
scopeModulePath = ModulePath () . NonEmpty.map (Symbol ()) . absScopePath

-- | The scope of a module with this declaration.
moduleDeclScope :: ModuleDecl an -> AbsScope
moduleDeclScope (ModuleDecl _ path) = modulePathScope path

modulePathScope :: ModulePath an -> AbsScope
modulePathScope (ModulePath _ xs) = AbsScope $ NonEmpty.map symbolLiteral xs
