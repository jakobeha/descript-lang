module Descript.Free.Data.Atom
  ( module Descript.Free.Data.Atom.PropPath
  , module Descript.Lex.Data.Atom
  ) where

import Descript.Free.Data.Atom.PropPath
import Descript.Lex.Data.Atom
