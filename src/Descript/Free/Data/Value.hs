{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedStrings #-}

module Descript.Free.Data.Value
  ( Shortcut
  , Property (..)
  , Record (..)
  , Part (..)
  , Value (..)
  , valueToPropKey
  ) where

import Descript.Free.Data.Shortcut
import Descript.Free.Data.Atom
import Descript.Misc
import Data.Monoid
import Data.List.NonEmpty (NonEmpty (..))
import Prelude hiding (head)

-- | Syntax sugar - expands into a value or part.
type Shortcut an = GenShortcut Value an

-- | A free property. Can be refined into a property declaration for a
-- record type (as just a key), or an actual property (as a define).
data Property an
  = PropertySingle (Value an)
  | PropertyDef an (Symbol an) (Value an)
  deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | A free record. Can be refined into a regular record, or a record type.
data Record an
  = Record
  { recordAnn :: an
  , head :: Symbol an
  , properties :: [Property an]
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | A free part. Can be refined into a regular part, input part, or
-- output part.
data Part an
  = PartPrim (Prim an)
  | PartRecord (Record an)
  | PartPropPath (PropPath an)
  | PartShortcut (Shortcut an)
  deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

-- | A free value. Can be refined into a regular value, input value, or
-- output value.
data Value an
  = Value an [Part an]
  deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

instance Ann Value where
  getAnn (Value ann _) = ann

instance Ann Part where
  getAnn (PartPrim x) = getAnn x
  getAnn (PartRecord x) = getAnn x
  getAnn (PartPropPath x) = getAnn x
  getAnn (PartShortcut x) = getAnn x

instance Ann Record where
  getAnn = recordAnn

instance Ann Property where
  getAnn (PropertySingle x) = getAnn x
  getAnn (PropertyDef ann _ _) = ann

instance Printable Value where
  aprintRec sub (Value _ parts) = pintercal " | " $ map sub parts

instance Printable Part where
  aprintRec sub (PartPrim prim) = sub prim
  aprintRec sub (PartRecord record) = sub record
  aprintRec sub (PartPropPath path) = sub path
  aprintRec sub (PartShortcut cut) = sub cut

instance Printable Record where
  aprintRec sub record = sub (head record) <> propsPrinted
    where propsPrinted = "[" <> pintercal ", " propPrinteds <> "]"
          propPrinteds = map sub $ properties record

instance Printable Property where
  aprintRec sub (PropertySingle x) = sub x
  aprintRec sub (PropertyDef _ key val) = sub key <> ": " <> sub val

instance (Show an) => Summary (Value an) where
  summaryRec = pprintSummaryRec

instance (Show an) => Summary (Part an) where
  summaryRec = pprintSummaryRec

instance (Show an) => Summary (Record an) where
  summaryRec = pprintSummaryRec

instance (Show an) => Summary (Property an) where
  summaryRec = pprintSummaryRec

valueToPropKey :: Value an -> Maybe (Symbol an)
valueToPropKey (Value _ [PartPropPath (PropPath _ (PathElemImp sym :| []))])
  = Just sym
valueToPropKey _ = Nothing
