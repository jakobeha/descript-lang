-- | Expand syntax sugar (shortcuts).
module Descript.Free.Refine.Expand
  ( expandShortList
  ) where

import Descript.Free.Data
import Descript.Misc

-- | Expands a short list into a record.
--
-- >>> expandShortList "@[foo, bar, baz]"
-- "Cons[first: foo, rest: Cons[first: bar, rest: Cons[first: baz, rest: Nil[]]]]"
expandShortList :: an -> [Value an] -> Record an
expandShortList ann [] = Record ann (ann <$ nilSym) []
expandShortList ann (x : xs)
  = Record ann (ann <$ consSym)
  [ PropertyDef xAnn (xAnn <$ consFirstSym) x
  , PropertyDef ann (ann <$ consRestSym) xs' ]
  where xs' = Value ann [PartRecord $ expandShortList ann xs]
        xAnn = getAnn x
