-- | Defines "free" versions of datatypes, which are very general, and
-- can be refined to more constrained, specific datatypes.
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
