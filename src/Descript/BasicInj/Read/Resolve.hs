-- | Resolves dependencies
module Descript.BasicInj.Read.Resolve
  ( extraModule
  ) where

import Descript.BasicInj.Read.Resolve.Subst
import Descript.BasicInj.Traverse
import Descript.BasicInj.Data
import Descript.Misc

-- | Contains imported data used by the module.
extraModule :: (Monad u)
            => DepResolver u
            -> ImportCtx an
            -> DirtyDepT an u
extraModule rsvr (ImportCtx _ mdecl idecls)
  = optimNub . mconcat <$> traverse (resolveImportDep rsvr mascope) idecls
  where mascope = modulePathScope $ moduleDeclPath mdecl

-- | Resolves the dependency for the module with the given scope
-- specified by the given import declaration.
resolveImportDep :: (Monad u)
                 => DepResolver u
                 -> AbsScope
                 -> ImportDecl an
                 -> DirtyDepT an u
resolveImportDep rsvr mascope (ImportDecl _ ipath ups isrcs idsts)
    = substDstImports idsts_ . substSrcImports isrcs_ . shiftPhaseUpN upsNum
  <$> resolveImportDep' rsvr mascope ipath
  where isrcs_ = map remAnns $ isrcs
        idsts_ = map remAnns $ idsts
        -- Probably shouldn't have wrapped @PhaseUps@ and given
        -- annotations, but it barely matters either way.
        upsNum = phaseUpsNum ups

-- | Resolves the dependency for the module with the given scope
-- specified by the given import path - doesn't apply substitutions.
resolveImportDep' :: (Monad u)
                  => DepResolver u
                  -> AbsScope
                  -> ModulePath an
                  -> DirtyDepT an u
resolveImportDep' rsvr mascope ipath
  = resToDirtyMonT
  $ mapErrorT (TagdDepError iann iascope)
  $ resolveDep rsvr mascope iascope
  where iascope = modulePathScope ipath
        iann = getAnn ipath
