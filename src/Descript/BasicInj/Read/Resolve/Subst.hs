module Descript.BasicInj.Read.Resolve.Subst
  ( substSrcImports
  , substDstImports
  ) where

import Descript.BasicInj.Traverse
import Descript.BasicInj.Data
import Descript.Misc

-- | Locally substitues each imported @from@ record head with its
-- @to@ counterpart. Does this by directly substituting occurrences in
-- the module, instead of adding reducers to the top phase.
substSrcImports :: (TaintAnn an)
                => [ImportRecord ()]
                -> AModule an
                -> AModule an
substSrcImports = localSubstMany . map isrcToSubst

-- | Globally substitues each imported @to@ record head with its
-- @from@ counterpart. Does this by adding reducers to the top phase
-- which apply each substitution, and also substituting in the record
-- declarations.
substDstImports :: (TaintAnn an)
                => [ImportRecord ()]
                -> AModule an
                -> AModule an
substDstImports = globalSubstMany . map idstToSubst

isrcToSubst :: ImportRecord () -> Subst
isrcToSubst (ImportRecord () from to) = Subst SubstFrom from to

idstToSubst :: ImportRecord () -> Subst
-- Weird and confusing - maybe should have better naming.
idstToSubst (ImportRecord () from to) = Subst SubstTo to from
