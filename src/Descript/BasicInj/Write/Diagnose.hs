-- | Generates diagnostics (errors, warnings, and useful information)
-- for Descript code.
module Descript.BasicInj.Write.Diagnose
  ( diagnose
  ) where

import Descript.BasicInj.Process.Reduce
import Descript.BasicInj.Process.Validate
import Descript.BasicInj.Data
import Descript.Misc
import Data.Semigroup

-- | Generates diagnostics for source.
diagnose :: (Ord an, Semigroup an)
         => DirtyDepd Source an
         -> [Diagnostic an]
diagnose ddsrc
  = case validate ddsrc of
         Failure probs -> map DiagProblem probs
         Success dsrc -> diagnoseValid dsrc

diagnoseValid :: Depd Source an -> [Diagnostic an]
diagnoseValid (Depd extra src)
  = case src of
         SourceModule _ -> []
         SourceProgram prog -> [interpretDiag $ Depd extra prog]

interpretDiag :: Depd Program an -> Diagnostic an
interpretDiag dprog
  = DiagEval (getAnn $ dquery dprog) (pprint $ interpret_ dprog)
