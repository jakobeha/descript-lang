-- | Abstracts traversals over source ASTs.
module Descript.BasicInj.Traverse
  ( module Descript.BasicInj.Traverse.Misc
  , module Descript.BasicInj.Traverse.Traverse
  , module Descript.BasicInj.Traverse.Termed
  , TTerm
  ) where

import Descript.BasicInj.Traverse.Misc
import Descript.BasicInj.Traverse.Traverse
import Descript.BasicInj.Traverse.Termed
import Descript.BasicInj.Traverse.Term (TTerm)
