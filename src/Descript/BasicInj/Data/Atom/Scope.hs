{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}

module Descript.BasicInj.Data.Atom.Scope
  ( FSymbolScope (..)
  , FSymbol (..)
  , mkFSymbol
  , codeFSym
  , undefinedFSym
  ) where

import Descript.Lex.Data.Atom
import Descript.Misc

-- | Context about a module and how it relates to the current module.
data FSymbolScope
  = FSymbolScope
  { -- | How many phases the symbol was imported above symbols in the
    -- current module.
    fsymbolScopePhaseUps :: Int
    -- | The scope of the symbol's original module.
  , fsymbolScopeMod :: AbsScope
  } deriving (Eq, Ord, Read, Show)

-- | A symbol a module scope. Two 'FSymbol's aren't necessarily equal
-- even if they have the same name and print, unlike two 'Symbol's.
data FSymbol an
  = FSymbol
  { fsymbolScope :: FSymbolScope
  , fsymbol :: Symbol an
  } deriving (Eq, Ord, Read, Show, Functor, Foldable, Traversable)

instance Ann FSymbol where
  getAnn = getAnn . fsymbol

instance EAnn FSymbol where
  FSymbol xScope xSym `eappend` FSymbol yScope ySym
    | xScope /= yScope = error "symbols' scopes not equal"
    | otherwise = FSymbol xScope $ xSym `eappend` ySym

instance Printable FSymbol where
  aprint = aprint . fsymbol

instance (Show an) => Summary (FSymbol an) where
  summary = pprintSummary

-- | Creates an 'FSymbol' which isn't phase shifted.
mkFSymbol :: AbsScope -> Symbol an -> FSymbol an
mkFSymbol = FSymbol . FSymbolScope 0

-- | The record head for code blocks.
codeFSym :: FSymbol ()
codeFSym = mkFSymbol primitiveScope codeSym

-- | A symbol for an undefined value - e.g. an unresolved implicit
-- property.
undefinedFSym :: FSymbol ()
undefinedFSym = mkFSymbol undefinedScope undefinedSym
