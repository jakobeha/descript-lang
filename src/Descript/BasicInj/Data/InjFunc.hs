module Descript.BasicInj.Data.InjFunc
  ( InjFuncMeta (..)
  , InjFunc
  , lookupFuncMeta
  , flookupFuncMeta
  , forceLookupFuncMeta
  , lookupFuncParams
  , lookupFunc
  , flookupFunc
  , forceLookupFunc
  ) where

import Descript.BasicInj.Data.Atom
import Descript.Misc
import Data.Monoid
import Data.Functor.Identity
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Fixed
import Prelude hiding (subtract, compare)
import qualified Prelude (compare)

-- | A function which operates on primtives, which can be accessed in
-- Descript. Contains information about what primitives it accepts.
data InjFuncMeta
  = InjFuncMeta
  { injFuncMetaParams :: [PrimType ()]
  , injFuncMetaFunc :: InjFunc
  }

-- | A function which operates on primtives, which can be accessed in
-- Descript. Returns 'Nothing' when given the wrong type of primitives.
type InjFunc = [Prim ()] -> Maybe (Prim ())

-- | All injected functions, with their corresponding identifiers.
-- (Identifiers not in 'InjSymbol' for convenience).
allFuncs :: Map String InjFuncMeta
allFuncs = Map.fromList
  [ ("Add", add)
  , ("Subtract", subtract)
  , ("Multiply", multiply)
  , ("DivideSafe", divideSafe)
  , ("ModuloSafe", moduloSafe)
  , ("RemainderSafe", remainderSafe)
  , ("Compare", compare)
  , ("AproxPi", aproxPi)
  , ("AproxExp", aproxExp)
  , ("AproxLog", aproxLog)
  , ("AproxSin", aproxSin)
  , ("AproxCos", aproxCos)
  , ("AproxTan", aproxTan)
  , ("AproxASin", aproxASin)
  , ("AproxACos", aproxACos)
  , ("AproxATan", aproxATan)
  , ("AproxATan2", aproxATan2)
  , ("Append", append)
  , ("App3", app3)
  ]

-- | Gets the injected function corresponding to the given identifer,
-- with metadata. Returns `Nothing` if the function doesn't exist.
lookupFuncMeta :: InjSymbol an -> Maybe InjFuncMeta
lookupFuncMeta (InjSymbol _ funcId) = allFuncs Map.!? funcId

-- | Gets the injected function corresponding to the given identifer,
-- with metadata. Fails if the function doesn't exist.
flookupFuncMeta :: (Monad w) => InjSymbol an -> w InjFuncMeta
flookupFuncMeta key
  = case lookupFuncMeta key of
         Nothing -> fail $ "no function with key: " ++ summary (remAnns key)
         Just x -> pure x

-- | Gets the injected function corresponding to the given identifer,
-- with metadata. Fails if the function doesn't exist.
forceLookupFuncMeta :: InjSymbol an -> InjFuncMeta
forceLookupFuncMeta = runIdentity . flookupFuncMeta

-- | Gets the parameter types of the injected function corresponding to
-- the given identifer. Returns `Nothing` if the function doesn't exist.
lookupFuncParams :: InjSymbol an -> Maybe [PrimType ()]
lookupFuncParams = fmap injFuncMetaParams . lookupFuncMeta

-- | Gets the injected function corresponding to the given identifer.
-- Returns `Nothing` if the function doesn't exist.
lookupFunc :: InjSymbol an -> Maybe InjFunc
lookupFunc = fmap injFuncMetaFunc . lookupFuncMeta

-- | Gets the injected function corresponding to the given identifer.
-- Fails if the function doesn't exist.
flookupFunc :: (Monad w) => InjSymbol an -> w InjFunc
flookupFunc = fmap injFuncMetaFunc . flookupFuncMeta

-- | Gets the injected function corresponding to the given identifer.
-- Fails if the function doesn't exist.
forceLookupFunc :: InjSymbol an -> InjFunc
forceLookupFunc = injFuncMetaFunc . forceLookupFuncMeta

add :: InjFuncMeta
add = InjFuncMeta addParams addFunc

addParams :: [PrimType ()]
addParams = [PrimTypeNumber (), PrimTypeNumber ()]

addFunc :: InjFunc
addFunc [PrimNumber () x, PrimNumber () y] = Just $ PrimNumber () $ x + y
addFunc _ = Nothing

subtract :: InjFuncMeta
subtract = InjFuncMeta subtractParams subtractFunc

subtractParams :: [PrimType ()]
subtractParams = [PrimTypeNumber (), PrimTypeNumber ()]

subtractFunc :: InjFunc
subtractFunc [PrimNumber () x, PrimNumber () y] = Just $ PrimNumber () $ x - y
subtractFunc _ = Nothing

multiply :: InjFuncMeta
multiply = InjFuncMeta multiplyParams multiplyFunc

multiplyParams :: [PrimType ()]
multiplyParams = [PrimTypeNumber (), PrimTypeNumber ()]

multiplyFunc :: InjFunc
multiplyFunc [PrimNumber () x, PrimNumber () y] = Just $ PrimNumber () $ x * y
multiplyFunc _ = Nothing

divideSafe :: InjFuncMeta
divideSafe = InjFuncMeta divideSafeParams divideSafeFunc

divideSafeParams :: [PrimType ()]
divideSafeParams = [PrimTypeNumber (), PrimTypeNumber (), PrimTypeNumber ()]

divideSafeFunc :: InjFunc
divideSafeFunc [PrimNumber () x, PrimNumber () y, PrimNumber () case0]
 = Just $ PrimNumber () $ divSafeNum x y case0
divideSafeFunc _ = Nothing

moduloSafe :: InjFuncMeta
moduloSafe = InjFuncMeta moduloSafeParams moduloSafeFunc

moduloSafeParams :: [PrimType ()]
moduloSafeParams = [PrimTypeNumber (), PrimTypeNumber (), PrimTypeNumber ()]

moduloSafeFunc :: InjFunc
moduloSafeFunc [PrimNumber () x, PrimNumber () y, PrimNumber () case0]
 = Just $ PrimNumber () $ modSafeNum x y case0
moduloSafeFunc _ = Nothing

remainderSafe :: InjFuncMeta
remainderSafe = InjFuncMeta remainderSafeParams remainderSafeFunc

remainderSafeParams :: [PrimType ()]
remainderSafeParams = [PrimTypeNumber (), PrimTypeNumber (), PrimTypeNumber ()]

remainderSafeFunc :: InjFunc
remainderSafeFunc [PrimNumber () x, PrimNumber () y, PrimNumber () case0]
 = Just $ PrimNumber () $ remSafeNum x y case0
remainderSafeFunc _ = Nothing

compare :: InjFuncMeta
compare = InjFuncMeta compareParams compareFunc

compareParams :: [PrimType ()]
compareParams = [PrimTypeNumber (), PrimTypeNumber ()]

compareFunc :: InjFunc
compareFunc [PrimNumber () x, PrimNumber () y] = Just $ PrimNumber () $ x `compareNum` y
compareFunc _ = Nothing

aproxPi :: InjFuncMeta
aproxPi = InjFuncMeta aproxPiParams aproxPiFunc

aproxPiParams :: [PrimType ()]
aproxPiParams = []

aproxPiFunc :: InjFunc
aproxPiFunc [] = Just $ PrimNumber () $ toRational (pi :: Double)
aproxPiFunc _ = Nothing

aproxExp :: InjFuncMeta
aproxExp = InjFuncMeta aproxExpParams aproxExpFunc

aproxExpParams :: [PrimType ()]
aproxExpParams = [PrimTypeNumber ()]

aproxExpFunc :: InjFunc
aproxExpFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ exp (fromRational x :: Double)
aproxExpFunc _ = Nothing

aproxLog :: InjFuncMeta
aproxLog = InjFuncMeta aproxLogParams aproxLogFunc

aproxLogParams :: [PrimType ()]
aproxLogParams = [PrimTypeNumber ()]

aproxLogFunc :: InjFunc
aproxLogFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ log (fromRational x :: Double)
aproxLogFunc _ = Nothing

aproxSin :: InjFuncMeta
aproxSin = InjFuncMeta aproxSinParams aproxSinFunc

aproxSinParams :: [PrimType ()]
aproxSinParams = [PrimTypeNumber ()]

aproxSinFunc :: InjFunc
aproxSinFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ sin (fromRational x :: Double)
aproxSinFunc _ = Nothing

aproxCos :: InjFuncMeta
aproxCos = InjFuncMeta aproxCosParams aproxCosFunc

aproxCosParams :: [PrimType ()]
aproxCosParams = [PrimTypeNumber ()]

aproxCosFunc :: InjFunc
aproxCosFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ cos (fromRational x :: Double)
aproxCosFunc _ = Nothing

aproxTan :: InjFuncMeta
aproxTan = InjFuncMeta aproxTanParams aproxTanFunc

aproxTanParams :: [PrimType ()]
aproxTanParams = [PrimTypeNumber ()]

aproxTanFunc :: InjFunc
aproxTanFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ tan (fromRational x :: Double)
aproxTanFunc _ = Nothing

aproxASin :: InjFuncMeta
aproxASin = InjFuncMeta aproxASinParams aproxASinFunc

aproxASinParams :: [PrimType ()]
aproxASinParams = [PrimTypeNumber ()]

aproxASinFunc :: InjFunc
aproxASinFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ asin (fromRational x :: Double)
aproxASinFunc _ = Nothing

aproxACos :: InjFuncMeta
aproxACos = InjFuncMeta aproxACosParams aproxACosFunc

aproxACosParams :: [PrimType ()]
aproxACosParams = [PrimTypeNumber ()]

aproxACosFunc :: InjFunc
aproxACosFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ acos (fromRational x :: Double)
aproxACosFunc _ = Nothing

aproxATan :: InjFuncMeta
aproxATan = InjFuncMeta aproxATanParams aproxATanFunc

aproxATanParams :: [PrimType ()]
aproxATanParams = [PrimTypeNumber ()]

aproxATanFunc :: InjFunc
aproxATanFunc [PrimNumber () x]
  = Just $ PrimNumber () $ toRational $ atan (fromRational x :: Double)
aproxATanFunc _ = Nothing

aproxATan2 :: InjFuncMeta
aproxATan2 = InjFuncMeta aproxATan2Params aproxATan2Func

aproxATan2Params :: [PrimType ()]
aproxATan2Params = [PrimTypeNumber (), PrimTypeNumber ()]

aproxATan2Func :: InjFunc
aproxATan2Func [PrimNumber () x, PrimNumber () y]
  = Just $ PrimNumber () $ toRational
  $ atan2 (fromRational x :: Double) (fromRational y :: Double)
aproxATan2Func _ = Nothing

append :: InjFuncMeta
append = InjFuncMeta appendParams appendFunc

appendParams :: [PrimType ()]
appendParams = [PrimTypeText (), PrimTypeText ()]

appendFunc :: InjFunc
appendFunc [PrimText () x, PrimText () y] = Just $ PrimText () $ x <> y
appendFunc _ = Nothing

app3 :: InjFuncMeta
app3 = InjFuncMeta app3Params app3Func

app3Params :: [PrimType ()]
app3Params = [PrimTypeText (), PrimTypeText (), PrimTypeText ()]

app3Func :: InjFunc
app3Func [PrimText () x, PrimText () y, PrimText () z]
  = Just $ PrimText () $ x <> y <> z
app3Func _ = Nothing

divSafeNum :: Rational -> Rational -> Rational -> Rational
divSafeNum x y case0
  | y == 0 = case0
  | otherwise = x / y

modSafeNum :: Rational -> Rational -> Rational -> Rational
modSafeNum x y case0
  | y == 0 = case0
  | otherwise = x `mod'` y

remSafeNum :: Rational -> Rational -> Rational -> Rational
remSafeNum x y case0
  | y == 0 = case0
  | otherwise = x `rem'` y

compareNum :: Rational -> Rational -> Rational
x `compareNum` y
  = case x `Prelude.compare` y of
         LT -> -1
         GT -> 1
         EQ -> 0

rem' :: Real a => a -> a -> a
x `rem'` y
  | modRes < 0 = negate y + modRes
  | otherwise = modRes
  where modRes = x `mod'` y
