-- | Shift reducer phase levels.
module Descript.BasicInj.Traverse.Misc.Phase
  ( shiftPhaseUpN
  ) where

import Descript.BasicInj.Traverse.Misc.Phase.Direct
import Descript.BasicInj.Traverse.Misc.Phase.Scope
import Descript.BasicInj.Data
import Descript.Misc

-- | Increases the phase level of all reducers. Also modifies symbols'
-- scopes so imported symbols don't work differently in other modules
-- when they're combined with this module.
shiftPhaseUpN :: (TaintAnn an)
              => Int
              -> AModule an
              -> AModule an
shiftPhaseUpN n = shiftPhaseUpNScope n . shiftPhaseUpNDirect n
