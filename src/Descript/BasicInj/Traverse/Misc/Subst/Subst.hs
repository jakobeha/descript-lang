{-# LANGUAGE TypeFamilies #-}

module Descript.BasicInj.Traverse.Misc.Subst.Subst
  ( SubstPart (..)
  , Subst (..)
  , substDeclHead
  ) where

import Descript.BasicInj.Data
import Core.Data.List.Assoc

data SubstPart
  = SubstFrom
  | SubstTo
  deriving (Eq, Ord, Read, Show)

data Subst
  = Subst SubstPart (FSymbol ()) (FSymbol ())
  deriving (Eq, Ord, Read, Show)

instance AssocPair Subst where
  type Key Subst = FSymbol ()
  type Value Subst = FSymbol ()

  getKey (Subst SubstFrom from _) = from
  getKey (Subst SubstTo _ to) = to
  getValue (Subst SubstFrom _ to) = to
  getValue (Subst SubstTo from _) = from

substDeclHead :: Subst -> FSymbol ()
substDeclHead = getKey
