{-# LANGUAGE TypeFamilies #-}

module Descript.BasicInj.Traverse.Misc.Phase.Scope
  ( shiftPhaseUpNScope
  ) where

import qualified Descript.BasicInj.Traverse.Term as T
import Descript.BasicInj.Traverse.Traverse
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc
import Data.Semigroup
import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NonEmpty

newtype ShiftPhaseScopeUpN an = ShiftPhaseScopeUpN Int

instance (TaintAnn an) => Mapping (ShiftPhaseScopeUpN an) where
  type MAnn (ShiftPhaseScopeUpN an) = an

  monTerm T.RecordHead (ShiftPhaseScopeUpN n) x
    = shiftPhaseScopeUpNFSymbol n x
  monTerm _ _ x = x

-- | Increases the phase level in the scopes of all symbols, and adds
-- reducers to their original phases which raise them.
shiftPhaseUpNScope :: (TaintAnn an, Semigroup an)
                   => Int
                   -> AModule an
                   -> AModule an
shiftPhaseUpNScope n amod
  = shiftPhaseScopeUpN n amod `appGend` bridgePhaseScopeUpN n amod_
  where amod_ = remAnns amod

-- | Increases the phase level in the scopes of all symbols, without
-- adding reducers.
shiftPhaseScopeUpN :: (TaintAnn an)
                   => Int
                   -> AModule an
                   -> AModule an
shiftPhaseScopeUpN = mapTerm T.AModule . ShiftPhaseScopeUpN

shiftPhaseScopeUpNFSymbol :: (TaintAnn an)
                          => Int
                          -> FSymbol an
                          -> FSymbol an
shiftPhaseScopeUpNFSymbol n (FSymbol scope sym)
  = FSymbol (shiftPhaseScopeUpNScope n scope) sym

shiftPhaseScopeUpNScope :: Int
                        -> FSymbolScope
                        -> FSymbolScope
shiftPhaseScopeUpNScope n (FSymbolScope ups path)
  = FSymbolScope (n + ups) path

-- | Creates a module with record declarations and reducers, which shift
-- phase-shifted symbols from this module back to their original phases.
bridgePhaseScopeUpN :: Int
                    -> AModule ()
                    -> AModule ()
-- Probably don't need to add record types.
bridgePhaseScopeUpN n amod
  = AModule
  { amoduleAnn = ()
    -- Want to almost duplicate when combined with shifted module.
  , recordCtx = recordCtx amod
  , reduceCtx = bridgePhaseScopeUpN' n $ recordCtx amod
  }

-- | Creates a context with reducers, which shift phase-shifted symbols
-- from this context back to their original phases.
bridgePhaseScopeUpN' :: Int
                     -> RecordCtx ()
                     -> ReduceCtx ()
bridgePhaseScopeUpN' n recCtx
  | n == 0 = mempty
  | otherwise
  = ReduceCtx () mempty
  $ NonEmpty.reverse
  $ fullPhase :| replicate (n - 1) mempty
  where fullPhase = PhaseCtx () $ bridgePhaseScopeUpNReducers n recCtx_
        recCtx_ = remAnns recCtx

bridgePhaseScopeUpNReducers :: Int
                            -> RecordCtx ()
                            -> [Reducer ()]
bridgePhaseScopeUpNReducers n (RecordCtx () decls)
  = map (bridgePhaseScopeUpNReducer n . recordDeclType) decls

bridgePhaseScopeUpNReducer :: Int
                           -> RecordType ()
                           -> Reducer ()
bridgePhaseScopeUpNReducer n (RecordType () oldHead propKeys)
  = Reducer () False old new
  where old
          = singletonValue
          $ In.PartRecord
          $ Record () oldHead
          $ map In.fullConsumeProp propKeys
        new
          = singletonValue
          $ Out.PartRecord
          $ Record () newHead
          $ map (Out.fullProduceProp oldHead) propKeys
        newHead = shiftPhaseScopeUpNFSymbol n oldHead
