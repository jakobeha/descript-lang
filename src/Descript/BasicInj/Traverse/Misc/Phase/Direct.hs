module Descript.BasicInj.Traverse.Misc.Phase.Direct
  ( shiftPhaseUpNDirect
  ) where

import Descript.BasicInj.Data
import Descript.Misc
import qualified Core.Data.List.NonEmpty as NonEmpty

-- | Increases the phase level of all reducers, without modifying their
-- scopes. Imported symbols would work differently in other modules when
-- they're combined with this module.
shiftPhaseUpNDirect :: (TaintAnn an)
                    => Int
                    -> AModule an
                    -> AModule an
shiftPhaseUpNDirect n (AModule ann recCtx redCtx)
  = AModule ann recCtx $ shiftPhaseUpNDirectReduceCtx n redCtx


shiftPhaseUpNDirectReduceCtx :: (TaintAnn an)
                             => Int
                             -> ReduceCtx an
                             -> ReduceCtx an
shiftPhaseUpNDirectReduceCtx n (ReduceCtx ann top phases)
  = ReduceCtx ann top $ phases NonEmpty.+| replicate n emptyPhase
  where emptyPhase = postInsertAnn ann <$ (mempty :: PhaseCtx ())
