{-# LANGUAGE TypeFamilies #-}

-- | Change scopes in symbols.
module Descript.BasicInj.Traverse.Misc.Scope
  ( globalSubstModScope
  , globalSubstScope
  ) where

import Descript.BasicInj.Traverse.Misc.Subst
import qualified Descript.BasicInj.Traverse.Term as T
import Descript.BasicInj.Traverse.Traverse
import Descript.BasicInj.Data
import Descript.Misc
import Data.List

data SubstScope an = SubstScope AbsScope AbsScope

instance Fold (SubstScope an) where
  type FAnn (SubstScope an) = an
  type Res (SubstScope an) = [Subst]

  fonTerm T.RecordHead (SubstScope old new) x
    = substScopeIndiv old new x
  fonTerm _ _ _ = []

-- | Substitutes the given scope with this module's scope, in all this
-- module's declared record heads. Used by module directories.
globalSubstModScope :: (TaintAnn an) => AbsScope -> Source an -> Source an
globalSubstModScope oldScope src
  = globalSubstScope oldScope newScope src
  where newScope = sourceScope src

-- | Globally substitutes the first scope with the second, in all this
-- module's declared record heads.
globalSubstScope :: (TaintAnn an)
                 => AbsScope
                 -> AbsScope
                 -> Source an
                 -> Source an
globalSubstScope oldScope newScope src
  = globalSubstManySource (substScope oldScope newScope src) src

substScope :: AbsScope
              -> AbsScope
              -> Source an
              -> [Subst]
substScope oldScope newScope
  = nub
  . foldTerm T.RecordCtx (SubstScope oldScope newScope)
  . recordCtx
  . sourceAModule

substScopeIndiv :: AbsScope -> AbsScope -> FSymbol an -> [Subst]
substScopeIndiv old new (FSymbol scope x)
  | fsymbolScopeMod scope /= new = []
  | otherwise = [Subst SubstTo (mkFSymbol old x_) (mkFSymbol new x_)]
  where x_ = remAnns x
