{-# LANGUAGE TypeFamilies #-}

-- | Make processing source code efficient, without changing semantics.
module Descript.BasicInj.Traverse.Misc.Optimize
  ( optimNub
  , optimNubReduceCtx
  ) where

import qualified Descript.BasicInj.Traverse.Term as T
import Descript.BasicInj.Traverse.Traverse
import Descript.BasicInj.Data
import Data.List

data OptimNub = OptimNub

instance Mapping OptimNub where
  type MAnn OptimNub = ()

  monTerm T.RecordCtx OptimNub (RecordCtx () decls)
    = RecordCtx () $ nub decls
  monTerm T.PhaseCtx OptimNub (PhaseCtx () reds)
    = PhaseCtx () $ nub reds
  monTerm _ _ x = x

-- | Optimize processing source code by removing redundant declarations
-- and reducers.
optimNub :: AModule () -> AModule ()
optimNub = mapTerm T.AModule OptimNub

-- | Optimize processing source code by removing redundant reducers.
optimNubReduceCtx :: ReduceCtx () -> ReduceCtx ()
optimNubReduceCtx = mapTerm T.ReduceCtx OptimNub
