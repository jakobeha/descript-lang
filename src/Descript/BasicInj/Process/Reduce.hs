-- | Reduction algorithm.
module Descript.BasicInj.Process.Reduce
  ( interpret_
  ) where

import qualified Descript.BasicInj.Process.Reduce.NoAnn as NoAnn
import qualified Descript.BasicInj.Data.Value.Reg as Reg
import Descript.BasicInj.Data.Source
import Descript.Misc

-- | Removes all annotations, then interprets the program.
interpret_ :: Depd Program an -> Reg.Value ()
interpret_ = NoAnn.interpret . fmap remAnns
