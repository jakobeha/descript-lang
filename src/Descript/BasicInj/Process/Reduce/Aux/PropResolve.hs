module Descript.BasicInj.Process.Reduce.Aux.PropResolve
  ( forceResolve
  , forceResolveIn'
  , forceResolvePropPath
  , forceResolveInPropPath
  , fresolve
  , fresolveIn'
  , fresolvePropPath
  , fresolveInPropPath
  ) where

import Descript.BasicInj.Process.Reduce.Aux.NormReducePart
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc
import Data.Monoid
import Data.Functor.Identity
import Data.Foldable
import Data.Proxy
import Data.Maybe
import Data.List
import qualified Data.List.NonEmpty as NonEmpty
import Control.Monad
import Prelude hiding (mod)

-- | Resolves all property paths in the output value using the given value.
-- Replaces all paths with the corresponding properties in the given
-- value. Fails if the value can't be resolved (e.g. a property doesn't
-- exist).
forceResolve :: (GenNormReducePart p)
             => Out.Value ()
             -> GenValue p ()
             -> GenValue p ()
forceResolve output' = runIdentity . fresolve output'

forceResolveIn' :: Out.Value () -> In.Value () -> In.Value ()
forceResolveIn' output' = runIdentity . fresolveIn' output'

-- | Resolves the property path using the given value.
-- Replaces it with the corresponding property in the given value.
-- Fails if the value can't be resolved (e.g. the property doesn't
-- exist).
forceResolvePropPath :: (GenNormReducePart p)
                     => PropPath ()
                     -> GenValue p ()
                     -> GenValue p ()
forceResolvePropPath path = runIdentity . fresolvePropPath path

forceResolveInPropPath :: PropPath () -> In.OptValue () -> In.OptValue ()
forceResolveInPropPath path = runIdentity . fresolveInPropPath path

-- | Resolves all property paths in the output value using the given value.
-- Replaces all paths with the corresponding properties in the given
-- value.
fresolve :: (Monad w, GenNormReducePart p)
         => Out.Value ()
         -> GenValue p ()
         -> w (GenValue p ())
fresolve (Value () outParts) value
  = mconcat <$> traverse fresolvePart outParts
  where fresolvePart (Out.PartPrim prim)
          = pure $ singletonValue $ primToPart Proxy prim
        fresolvePart (Out.PartRecord record)
            = singletonValue
            . recToPart Proxy
          <$> traversePropVals (`fresolve` value)
              record
        fresolvePart (Out.PartPropPath path) = fresolvePropPath path value
        fresolvePart (Out.PartInjApp app) = fresolveInjApp app value

fresolveIn' :: (Monad w) => Out.Value () -> In.Value () -> w (In.Value ())
fresolveIn' output' value = do
  res <- fresolveIn output' $ In.JustValue value
  case res of
    In.NothingValue -> fail "Bad macro - reduces input to free bind"
    In.JustValue x -> pure x

fresolveIn :: (Monad w)
           => Out.Value ()
           -> In.OptValue ()
           -> w (In.OptValue ())
fresolveIn (Value () outParts) value
  = mconcat <$> traverse fresolvePart outParts
  where fresolvePart (Out.PartPrim prim)
          = pure $ In.JustValue $ singletonValue $ primToPart Proxy prim
        fresolvePart (Out.PartRecord record)
            = In.JustValue
            . singletonValue
            . recToPart Proxy
          <$> traversePropVals (`fresolveIn` value)
              record
        fresolvePart (Out.PartPropPath path)
          = fresolveInPropPath path value
        fresolvePart (Out.PartInjApp app)
          = In.JustValue <$> fresolveInInjApp app value

-- | Resolves the property path using the given value.
-- Replaces it with the corresponding property in the given value.
fresolvePropPath :: (Monad w, GenNormReducePart p)
                 => PropPath ()
                 -> GenValue p ()
                 -> w (GenValue p ())
fresolvePropPath (PropPath () xs) = fresolveSubpath $ NonEmpty.toList xs

fresolveInPropPath :: (Monad w)
                   => PropPath ()
                   -> In.OptValue ()
                   -> w (In.OptValue ())
fresolveInPropPath (PropPath () xs) = fresolveInSubpath $ NonEmpty.toList xs

fresolveSubpath :: (Monad w, GenNormReducePart p)
                => [PathElem ()]
                -> GenValue p ()
                -> w (GenValue p ())
fresolveSubpath [] = pure
fresolveSubpath (x : xs) = fresolveSubpath xs <=< fresolveElem x

fresolveInSubpath :: (Monad w)
                  => [PathElem ()]
                  -> In.OptValue ()
                  -> w (In.OptValue ())
fresolveInSubpath [] = pure
fresolveInSubpath (x : xs) = fresolveInSubpath xs <=< fresolveInElem x

fresolveElem :: (Monad w, GenNormReducePart p)
             => PathElem ()
             -> GenValue p ()
             -> w (GenValue p ())
fresolveElem (PathElem () keyRef' headRef')
  = flookupProp keyRef' <=< frecWithHead headRef'

fresolveInElem :: (Monad w)
               => PathElem ()
               -> In.OptValue ()
               -> w (In.OptValue ())
fresolveInElem _ In.NothingValue
  = fail "Bad macro - references property of free bind"
fresolveInElem (PathElem () keyRef' headRef') (In.JustValue val)
  = flookupProp keyRef' =<< frecWithHead headRef' val

-- | Finds the injected function, fresolves the arguments, then applies
-- the function with every possible primitive combination until it
-- returns a result.
fresolveInjApp :: (Monad w, GenNormReducePart p)
               => Out.InjApp ()
               -> GenValue p ()
               -> w (GenValue p ())
fresolveInjApp app x = do
  func <- flookupFunc $ Out.funcId app
  params <- traverse ((`fresolve` x) . Out.injParamVal) $ Out.params app
  fapplyInj func params

fresolveInInjApp :: (Monad w)
                 => Out.InjApp ()
                 -> In.OptValue ()
                 -> w (In.Value ())
fresolveInInjApp app x = do
  func <- flookupFunc $ Out.funcId app
  params <- mapMaybe In.optValToMaybeVal
    <$> traverse ((`fresolveIn` x) . Out.injParamVal) (Out.params app)
  fapplyInj func params

-- Applies the function with every possible primitive combination given
-- the arguments until it returns a result. Fails if no combinations work.
fapplyInj :: (Monad w, GenPart p)
         => InjFunc
         -> [GenValue p ()]
         -> w (GenValue p ())
fapplyInj func params
  = case tryApplyInj func params of
         Failure () -> fail "Injected function couldn't be applied to parameters"
         Success out -> pure out

-- Applies the function with every possible primitive combination given
-- the arguments until it returns a result.
tryApplyInj :: (GenPart p)
            => InjFunc
            -> [GenValue p ()]
            -> UResult (GenValue p ())
tryApplyInj = tryApplyInjUsing []

tryApplyInjUsing :: (GenPart p)
              => [Prim ()]
              -> InjFunc
              -> [GenValue p ()]
              -> UResult (GenValue p ())
tryApplyInjUsing revSelParams func []
  = case func selParams of
         Nothing -> Failure ()
         Just out -> Success $ singletonValue $ primToPart Proxy out
  where selParams = reverse revSelParams
tryApplyInjUsing revSelParams func (nextParam : restParams)
  = asum $ map tryApplyInjUsingNext $ primParts nextParam
  where tryApplyInjUsingNext nextSelParam
          = tryApplyInjUsing (nextSelParam : revSelParams) func restParams
