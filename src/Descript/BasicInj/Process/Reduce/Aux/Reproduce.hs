-- | Derives output values from input values, which produce what the
-- input value consumes.
module Descript.BasicInj.Process.Reduce.Aux.Reproduce
  ( reproduce
  , reproduceStaticOpt
  , reproduceStatic
  ) where

import Descript.BasicInj.Process.Reduce.Aux.NormReducePart
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Data.Proxy
import Data.Maybe

-- | The result will re-produce everything which was consumed by the
-- input. It's semantically equivalent to an empty property path, but
-- those don't exist. Note that a top-level primitive type has no full
-- output (you can't re-produce it), so that will raise an error.
reproduce :: In.Value () -> Out.Value ()
reproduce (Value () parts) = Value () $ map reproducePart parts

-- | The result will re-produce everything /static/ which was consumed
-- by the input. In particular, it won't have any property paths, which
-- is why it can be a regular value, not just an output. This loses
-- information - full matches and primitive types.
reproduceStaticOpt :: (GenNormReducePart p) => Proxy p -> In.OptValue () -> GenValue p ()
reproduceStaticOpt _ In.NothingValue = mempty
reproduceStaticOpt pxy (In.JustValue val) = reproduceStatic pxy val

-- | The result will re-produce everything /static/ which was consumed
-- by the input. In particular, it won't have any property paths, which
-- is why it can be any type of value, not just an output. This loses
-- information - full matches and primitive types.
reproduceStatic :: (GenNormReducePart p) => Proxy p -> In.Value () -> GenValue p ()
reproduceStatic pxy (Value () parts)
  = Value () $ mapMaybe (reproduceStaticPart pxy) parts

reproducePart :: In.Part () -> Out.Part ()
reproducePart (In.PartPrim prim) = Out.PartPrim prim
reproducePart (In.PartPrimType _)
  = error "Top-level primitive type has no full output - you can't \
          \reproduce it in an output value, because it's not a single \
          \value and it doesn't correspond to a property path."
reproducePart (In.PartRecord record) = Out.PartRecord $ reproduceRecord record

reproduceStaticPart :: (GenNormReducePart p) => Proxy p -> In.Part () -> Maybe (p ())
reproduceStaticPart pxy (In.PartPrim prim) = Just $ primToPart pxy prim
reproduceStaticPart _ (In.PartPrimType _) = Nothing
reproduceStaticPart pxy (In.PartRecord record)
  = Just $ recToPart pxy $ reproduceStaticRecord pxy record

reproduceRecord :: In.Record () -> Out.Record ()
reproduceRecord (Record () head' props)
  = Record () head' $ map (reproduceProp head') props

reproduceStaticRecord :: (GenNormReducePart p) => Proxy p -> In.Record () -> PartRecord p ()
reproduceStaticRecord pxy (Record () head' props)
  = Record () head' $ map (reproduceStaticProp pxy) props

reproduceProp :: FSymbol () -> In.Property () -> Out.Property ()
reproduceProp head' (Property () key _) = Property () key $ Out.immPathVal elem'
  where elem' = PathElem () key head'

reproduceStaticProp :: (GenNormReducePart p) => Proxy p -> In.Property () -> PartProperty p ()
reproduceStaticProp pxy (Property () key val)
  = Property () key $ reproduceStaticOpt pxy val
