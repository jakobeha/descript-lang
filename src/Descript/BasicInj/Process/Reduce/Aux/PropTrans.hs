{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TupleSections #-}

module Descript.BasicInj.Process.Reduce.Aux.PropTrans
  ( PropTranses
  , PropTrans (..)
  , transProps
  , apPropTranses
  , subPropTranses
  ) where

import Descript.BasicInj.Process.Reduce.Aux.PropResolve
import Descript.BasicInj.Process.Reduce.Aux.Reproduce
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc
import Data.Proxy
import Data.Maybe
import Data.List
import Data.List.NonEmpty (NonEmpty (..))
import Control.Applicative

-- | Replaces properties within a value.
type PropTranses = [PropTrans]

-- | Replaces every occurrence of a property path with a union of the
-- new subpaths. If a subpath is empty, it's replaced by the entire
-- input value with immediate properties. Also contains a reference to
-- the input value it was derived from.
data PropTrans
  = PropTrans
  { propTransInput :: In.Value ()
  , propTransOld :: PropPath ()
  , propTransNews :: [SubPropPath ()]
  } deriving (Show)

transProps :: Out.Value () -> In.Value () -> PropTranses
transProps output' input'
  = map (transPath output' input') $ pathsInVal input'

pathsInVal :: In.Value () -> [PropPath ()]
pathsInVal (Value () parts) = concatMap pathsInPart parts

pathsInPart :: In.Part () -> [PropPath ()]
pathsInPart (In.PartPrim _) = []
pathsInPart (In.PartPrimType _) = []
pathsInPart (In.PartRecord record) = pathsInRecord record

pathsInRecord :: In.Record () -> [PropPath ()]
pathsInRecord (Record () head' props) = concatMap (pathsInProp head') props

pathsInProp :: FSymbol ()
            -> In.Property ()
            -> [PropPath ()]
pathsInProp head' (Property () key val)
  = immPath elem' : map (subPath elem') (pathsInOptVal val)
  where elem' = PathElem () key head'

pathsInOptVal :: In.OptValue () -> [PropPath ()]
pathsInOptVal In.NothingValue = []
pathsInOptVal (In.JustValue x) = pathsInVal x

transPath :: Out.Value () -> In.Value () -> PropPath () -> PropTrans
transPath output' input' inPath
  = PropTrans
  { propTransInput = input'
  , propTransOld = inPath
  , propTransNews = transOutsInVal inPath output'
  }

transOutsInVal :: PropPath () -> Out.Value () -> [SubPropPath ()]
transOutsInVal inPath (Value () parts) = concatMap (transOutsInPart inPath) parts

transOutsInPart :: PropPath () -> Out.Part () -> [SubPropPath ()]
transOutsInPart _ (Out.PartPrim _) = []
transOutsInPart inPath (Out.PartRecord record)
  = transOutsInRecord inPath record
transOutsInPart inPath (Out.PartPropPath path)
  = transOutsInPath inPath path
transOutsInPart inPath (Out.PartInjApp app)
  = transOutsInInjApp inPath app

transOutsInRecord :: PropPath () -> Out.Record () -> [SubPropPath ()]
transOutsInRecord inPath (Record () head' props)
  = concatMap (transOutsInProp inPath head') props

transOutsInProp :: PropPath () -> FSymbol () -> Out.Property () -> [SubPropPath ()]
transOutsInProp inPath head' (Property () key val)
  = map (elem' :) $ transOutsInVal inPath val
  where elem' = PathElem () key head'

-- | Name is misleading out of context - the old property path will
-- transform into the location of this property path if both paths are
-- equal.
transOutsInPath :: PropPath () -> PropPath () -> [SubPropPath ()]
transOutsInPath inPath path
  | inPath == path = [[]]
  | otherwise = []

transOutsInInjApp :: PropPath () -> Out.InjApp () -> [SubPropPath ()]
transOutsInInjApp _ _
  = error "Macros deconstructing injected applications is unsupported. \
          \Wrap the injected application in a regular record, then \
          \deconstruct that record instead."


-- | Merges the transformations, then applies them all.
apPropTranses :: (TaintAnn an)
              => In.Value ()
              -> PropTranses
              -> Out.Value an
              -> Out.Value an
apPropTranses in' transes x
  = foldl' (flip $ apPropTrans in') x $ mergeTranses transes

-- | Adds outputs from more general transes to more specific ones.
-- For example:
--
-- >>> mergeTranses ['a>c>d -> ', 'a -> z', 'a>b -> y']
-- ['a>c>d -> z>c>d', 'a -> z', 'a>b -> y | z>b']
mergeTranses :: PropTranses -> PropTranses
mergeTranses = foldl' (flip mergeAddTrans) []

-- | Adds outputs from more general transes to the new trans, and adds
-- outputs from the new trans to more specific ones.
mergeAddTrans :: PropTrans -> PropTranses -> PropTranses
mergeAddTrans x [] = [x]
mergeAddTrans x (y : ys) = y' : mergeAddTrans x' ys
  where (x', y') = tryMergeTrans x y

-- | If one of the transes is a prefix of the other, returns both so
-- the longer one has the smaller one's outputs. Otherwise returns both
-- unaffected.
tryMergeTrans :: PropTrans -> PropTrans -> (PropTrans, PropTrans)
tryMergeTrans x y = (x, y) `fromMaybe` mergeTrans x y

-- | If one of the transes is a prefix of the other, returns both so
-- the longer one has the smaller one's outputs.
mergeTrans :: PropTrans -> PropTrans -> Maybe (PropTrans, PropTrans)
mergeTrans x y
    = (x, ) <$> mergeTrans1Way x y
  <|> (, y) <$> mergeTrans1Way y x

-- | If the first transes is a prefix of the second, returns the second
-- so it includes the first. Otherwise returns 'Nothing'.
mergeTrans1Way :: PropTrans -> PropTrans -> Maybe PropTrans
mergeTrans1Way (PropTrans _ xOld xNews) (PropTrans yInput yOld yNews)
  = case xOld `stripPrefixPath` yOld of
         Nothing -> Nothing
         Just yRest -> Just $ PropTrans yInput yOld $ yNews ++ xNews'
           where xNews' = map (++ yRest) xNews

-- | Assumes there is a transformation for every reasonable path, and
-- the transformations were all merged (more general transformation
-- outputs, specified, were added to more specific outputs).
apPropTrans :: (TaintAnn an)
            => In.Value ()
            -> PropTrans
            -> Out.Value an
            -> Out.Value an
apPropTrans in' trans (Value ann parts)
  = reconValue ann parts $ map (apPropTransToPart in' trans) parts

apPropTransToPart :: (TaintAnn an)
                  => In.Value ()
                  -> PropTrans
                  -> Out.Part an
                  -> Out.Value an
apPropTransToPart _ _ (Out.PartPrim prim) = singletonValue $ Out.PartPrim prim
apPropTransToPart in' trans (Out.PartRecord record)
  = singletonValue $ Out.PartRecord $ apPropTransToRecord in' trans record
apPropTransToPart in' trans (Out.PartPropPath path)
  = apPropTransToPath in' trans path
apPropTransToPart in' trans (Out.PartInjApp app)
  = singletonValue $ Out.PartInjApp $ apPropTransToInjApp in' trans app

apPropTransToRecord :: (TaintAnn an)
                    => In.Value ()
                    -> PropTrans
                    -> Out.Record an
                    -> Out.Record an
apPropTransToRecord in' trans (Record ann head' props)
  -- No need for reRecord because prop counts, and thus annotation, will
  -- always stay the same.
  = Record ann head' $ map (apPropTransToProp in' trans) props

apPropTransToProp :: (TaintAnn an)
                  => In.Value ()
                  -> PropTrans
                  -> Out.Property an
                  -> Out.Property an
apPropTransToProp in' trans (Property ann key val)
  -- No need for reProperty because value printability, and thus
  -- annotation, will always stay the same.
  = Property ann key $ apPropTrans in' trans val

apPropTransToInjApp :: (TaintAnn an)
                    => In.Value ()
                    -> PropTrans
                    -> Out.InjApp an
                    -> Out.InjApp an
apPropTransToInjApp in' trans (Out.InjApp ann funcId' params')
  = Out.InjApp ann funcId' $ map (apPropTransToInjParam in' trans) params'

apPropTransToInjParam :: (TaintAnn an)
                      => In.Value ()
                      -> PropTrans
                      -> Out.InjParam an
                      -> Out.InjParam an
apPropTransToInjParam in' trans (Out.InjParam ann val)
  = Out.InjParam ann $ apPropTrans in' trans val

apPropTransToPath :: (TaintAnn an)
                  => In.Value ()
                  -> PropTrans
                  -> PropPath an
                  -> Out.Value an
apPropTransToPath in' trans path
-- Assumes there is a transformation for every reasonable path, and the
-- transformations were all merged. Otherwise would need to check prefix
-- instead of full path.
  | path /@= old = singletonValue $ Out.PartPropPath path
  | otherwise = ann' <$ newVal
  where newVal = ann' <$ propTransNewVal in' trans
        old = propTransOld trans
        ann' = taint $ getAnn path

-- | The output of the transformation (what the path is transformed to).
propTransNewVal :: In.Value () -> PropTrans -> Out.Value ()
propTransNewVal in' (PropTrans oldIn old news)
  -- Reifies and tries to get static info if it can't get dynamic info.
  | null news
  = reproduceStaticOpt Proxy
  $ forceResolveInPropPath old
  $ In.JustValue oldIn
  | otherwise = mconcat $ map (subPathVal in') news

-- | An output value which refers to the given sub-path, given the
-- corresponding input. If the sub-path is empty, the value will refer
-- to the entire input (via 'fullOut'). Otherwise it will just contain a
-- single property path.
subPathVal :: In.Value () -> SubPropPath () -> Out.Value ()
subPathVal in' [] = reproduce in'
subPathVal _ (x : xs) = singletonValue $ Out.PartPropPath $ PropPath () $ x :| xs

-- | Prepends the element to all property paths in all transformations.
subPropTranses :: PathElem () -> PropTranses -> PropTranses
subPropTranses = map . subPropTrans

-- | Prepends the element to the input and all output paths.
subPropTrans :: PathElem () -> PropTrans -> PropTrans
subPropTrans x (PropTrans input' old news)
  = PropTrans
  { propTransInput = subInputVal x input'
  , propTransOld = subPath x old
  , propTransNews = map (x :) news
  }

subInputVal :: PathElem () -> In.Value () -> In.Value ()
subInputVal (PathElem () key head') val
  = singletonValue
  $ In.PartRecord
  $ Record () head' [Property () key $ In.JustValue val]
