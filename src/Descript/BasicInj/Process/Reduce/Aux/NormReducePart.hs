{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Descript.BasicInj.Process.Reduce.Aux.NormReducePart
  ( GenNormReducePart
  ) where

import qualified Descript.BasicInj.Data.Value.Reg as Reg
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc

class (GenPart p, Eq (p ()), Show (p ()), Printable p, (PartPropVal p ~ GenValue p)) => GenNormReducePart p where

instance GenNormReducePart Reg.Part where

instance GenNormReducePart Out.Part where
