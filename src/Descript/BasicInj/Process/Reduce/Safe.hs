-- | Reduction algorithm.
--
-- Returns 'Nothing' instead of throwing exceptions.
module Descript.BasicInj.Process.Reduce.Safe
  ( resolve
  , resolveIn'
  ) where

import Descript.BasicInj.Process.Reduce.Aux
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc

-- | Resolves all property paths in the output value using the given value.
-- Replaces all paths with the corresponding properties in the given
-- value.
resolve :: (GenNormReducePart p)
        => Out.Value ()
        -> GenValue p ()
        -> UResult (GenValue p ())
resolve output' = maybeToUResult . fresolve output'

resolveIn' :: Out.Value () -> In.Value () -> UResult (In.Value ())
resolveIn' output' = maybeToUResult . fresolveIn' output'
