{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

-- | Reduction algorithm.
--
-- Doesn't use or preserve annotations.
module Descript.BasicInj.Process.Reduce.NoAnn
  ( interpret
  , reducePhase
  , reduceReg
  ) where

import Descript.BasicInj.Process.Reduce.Aux
import Descript.BasicInj.Traverse
import qualified Descript.BasicInj.Data.Value.Reg as Reg
import qualified Descript.BasicInj.Data.Value.In as In
import qualified Descript.BasicInj.Data.Value.Out as Out
import Descript.BasicInj.Data
import Descript.Misc
import Data.Monoid
import Core.Data.Functor
import Data.Foldable
import Data.Proxy
import Data.Maybe
import Data.List
import Core.Data.List
import Core.Data.List.Assoc hiding (Value)
import qualified Data.List.NonEmpty as NonEmpty
import Control.Monad
import Core.Control.Monad.Trans
import Control.Monad.Trans.Writer
import Prelude hiding (mod)

class (GenNormReducePart p) => NormReducePart p where
  reducePartProps :: PhaseCtx () -> p () -> p ()

instance NormReducePart Reg.Part where
  reducePartProps _ (Reg.PartPrim prim) = Reg.PartPrim prim
  reducePartProps ctx (Reg.PartRecord record)
    = Reg.PartRecord $ reduceRecordProps ctx record

reduceInPartProps :: PhaseCtx ()
                  -> In.Part ()
                  -> WriterT PropTranses [] (In.Part ())
reduceInPartProps _ (In.PartPrim prim) = pure $ In.PartPrim prim
reduceInPartProps _ (In.PartPrimType ptype) = pure $ In.PartPrimType ptype
reduceInPartProps ctx (In.PartRecord record)
  = In.PartRecord <$> reduceInRecordProps ctx record

instance NormReducePart Out.Part where
  reducePartProps _ (Out.PartPrim prim) = Out.PartPrim prim
  reducePartProps ctx (Out.PartRecord record)
    = Out.PartRecord $ reduceOutRecordProps ctx record
  reducePartProps _ (Out.PartPropPath path) = Out.PartPropPath path
  reducePartProps ctx (Out.PartInjApp app)
    = Out.PartInjApp $ reduceInjAppProps ctx app

reduceRecordProps :: PhaseCtx () -> Reg.Record () -> Reg.Record ()
reduceRecordProps = mapPropVals . reduceReg

reduceInRecordProps :: PhaseCtx ()
                    -> In.Record ()
                    -> WriterT PropTranses [] (In.Record ())
reduceInRecordProps ctx (Record () head' props)
  = Record () head' <$> traverse (reduceInRecordProp ctx head') props

reduceInRecordProp :: PhaseCtx ()
                   -> FSymbol ()
                   -> In.Property ()
                   -> WriterT PropTranses [] (In.Property ())
reduceInRecordProp ctx head' (Property () key val)
    = Property () key <$> val'
  where val'
          = censor (subPropTranses pelem)
          $ In.traverseOptVal (reduceInput ctx) val
        pelem = PathElem () key head'

reduceOutRecordProps :: PhaseCtx () -> Out.Record () -> Out.Record ()
reduceOutRecordProps = mapPropVals . reduceOutput

reduceInjAppProps :: PhaseCtx () -> Out.InjApp () -> Out.InjApp ()
reduceInjAppProps = Out.mapInjAppParams . Out.mapInjParamVal . reduceOutput

-- | Interprets the program - reduces its query using its reducers.
interpret :: Depd Program () -> Reg.Value ()
interpret dprog = reduceReg phase0 qval
  where phase0 = reduceAppPhases $ optimNubReduceCtx $ reduceCtx $ dmodule dprog
        qval = queryVal $ dquery dprog

-- | Macro-reduces each of the phases using reducers from the above
-- phase, adding reducers from the above phase along the way, then
-- returns the last phase (which will reduce the query).
reduceAppPhases :: ReduceCtx () -> PhaseCtx ()
reduceAppPhases (ReduceCtx () t xs) = foldl' reduceAppPhase t $ NonEmpty.toList xs

-- | Macro-reduces the second phase using reducers from the first.
-- Then combines the phases.
reduceAppPhase :: PhaseCtx () -> PhaseCtx () -> PhaseCtx ()
reduceAppPhase mctx x = mctx <> reducePhase mctx x

-- | Macro-reduces the second phase using reducers from the first.
reducePhase :: PhaseCtx () -> PhaseCtx () -> PhaseCtx ()
reducePhase mctx (PhaseCtx () xs)
  = PhaseCtx () $ concatMap (reduceReducer mctx) xs

-- | Macro-reduces the input and output (ctx is macros).
reduceReducer :: PhaseCtx () -> Reducer () -> [Reducer ()]
reduceReducer ctx (Reducer () affd input' output')
  | not affd = [Reducer () affd input' output']
  | otherwise = reduceReducerOut ctx output' <$> reduceInput' ctx input'

-- | Continues reducing the output using effects from the reduced input,
-- then creates the reduced reducer.
reduceReducerOut :: PhaseCtx ()
                 -> Out.Value ()
                 -> (In.Value (), PropTranses)
                 -> Reducer ()
reduceReducerOut ctx output' (newIn, outTranses)
  = Reducer () True newIn newOut
  where newOut = reduceOutput ctx $ apPropTranses newIn outTranses output'

-- | Applies the context's reducers to the value, until they can't
-- be applied anymore.
reduceReg :: PhaseCtx () -> Reg.Value () -> Reg.Value ()
reduceReg = reduceNorm

-- | Applies the context's reducers to the output value, until they
-- can't be applied anymore.
reduceOutput :: PhaseCtx () -> Out.Value () -> Out.Value ()
reduceOutput = reduceNorm

-- | Applies the context's reducers to the value, until they can't be
-- applied anymore.
reduceNorm :: (NormReducePart p)
           => PhaseCtx ()
           -> GenValue p ()
           -> GenValue p ()
reduceNorm ctx = reduceRest reduceNorm ctx . reduceProps ctx

-- | Applies the context's reducers to the input value, until they can't
-- be applied anymore.
--
-- This also returns transformers which should be applied to the output.
-- Every time a record in the input matches, the corresponding property
-- in the output is transformed to a union of all the locations of that
-- property in the reducer's output.
reduceInput' :: PhaseCtx ()
            -> In.Value ()
            -> [(In.Value (), PropTranses)]
reduceInput' ctx = runWriterT . reduceInput ctx

reduceInput :: PhaseCtx ()
            -> In.Value ()
            -> WriterT PropTranses [] (In.Value ())
reduceInput ctx = reduceInRest reduceInput ctx <=< reduceInProps ctx

-- | Applies the context's reducers to the value's properties,
-- until they can't be applied anymore.
reduceProps :: (NormReducePart p) => PhaseCtx () -> GenValue p () -> GenValue p ()
reduceProps ctx (Value () parts)
  = Value () $ map (reducePartProps ctx) parts

reduceInProps :: PhaseCtx ()
              -> In.Value ()
              -> WriterT PropTranses [] (In.Value ())
reduceInProps ctx (Value () parts)
  = Value () <$> traverse (reduceInPartProps ctx) parts

-- | Applies the context's reducers to the value's head.
-- If the value reduced, applies the given reducer to reduce it more.
-- Otherwise just returns it as-is.
reduceRest :: (NormReducePart p)
           => (PhaseCtx () -> GenValue p () -> GenValue p ())
           -> PhaseCtx () -> GenValue p () -> GenValue p ()
reduceRest reduceMore ctx value
  = case reduceOnce ctx value of
         Failure () -> value
         Success next -> reduceMore ctx next

reduceInRest :: (PhaseCtx () -> In.Value () -> WriterT PropTranses [] (In.Value ()))
             -> PhaseCtx () -> In.Value () -> WriterT PropTranses [] (In.Value ())
reduceInRest reduceInMore ctx value
  = mapWriterT continue $ reduceInOnce ctx value
  where continue = concatMap (runWriterT . continue') . runResultT
        continue' (Failure ()) = pure value
        continue' (Success (next, effTrs)) = do
          tell effTrs
          reduceInMore ctx next

-- | Applies the context's reducers to the value once, returning a new
-- value if it reduced, or a failure if it couldn't be reduced at all.
reduceOnce :: (NormReducePart p)
           => PhaseCtx ()
           -> GenValue p ()
           -> UResult (GenValue p ())
reduceOnce (PhaseCtx () reducers) value
  = asum $ map (`reduceIndiv` value) reducers

reduceInOnce :: PhaseCtx ()
             -> In.Value ()
             -> WriterT PropTranses (ResultT () []) (In.Value ())
reduceInOnce (PhaseCtx () reducers) value
  = asum' $ map (`reduceInIndiv` value) reducers

asum' :: [WriterT PropTranses (ResultT () []) a]
      -> WriterT PropTranses (ResultT () []) a
asum' [] = WriterT $ ResultT [Failure ()]
asum' (x : xs) = WriterT $ ResultT $ do
  next <- runResultT $ runWriterT x
  case next of
    Failure () -> runResultT $ runWriterT $ asum' xs
    Success y -> [Success y]

-- | Applies the individual reducer to the value if it can be applied
-- /and produces a different value/. Otherwise returns a failure.
reduceIndiv :: (NormReducePart p)
            => Reducer ()
            -> GenValue p ()
            -> UResult (GenValue p ())
reduceIndiv reducer value = do
  next <- reduceIndiv' reducer value
  guard $ next /= value
  pure next

reduceInIndiv :: Reducer ()
              -> In.Value ()
              -> WriterT PropTranses (ResultT () []) (In.Value ())
reduceInIndiv reducer value = do
  next <- reduceInIndiv' reducer value
  guard' $ next /= value
  pure next

guard' :: Bool -> WriterT PropTranses (ResultT () []) ()
guard' False = WriterT $ ResultT [Failure ()]
guard' True = pure ()

-- | Applies the reducer to the value if it can be applied, /whether or
-- /not it produces a different value/. Otherwise returns a failure.
reduceIndiv' :: (NormReducePart p)
             => Reducer ()
             -> GenValue p ()
             -> UResult (GenValue p ())
reduceIndiv' (Reducer () _ input' output')
  = fmap (produce output') . consume input'

reduceInIndiv' :: Reducer ()
               -> In.Value ()
               -> WriterT PropTranses (ResultT () []) (In.Value ())
reduceInIndiv' (Reducer () _ input' output')
  = WriterT . fmap (produceIn output') . consumeIn input'

-- | Tries to match the input value to the given value.
consume :: (NormReducePart p)
        => In.Value ()
        -> GenValue p ()
        -> UResult (Match (GenValue p ()))
consume (Value () inParts) value
  = foldM (flip consumePartInMatch) (emptyMatch value) inParts

consumeIn :: In.Value ()
          -> In.Value ()
          -> UResultT [] (Match (In.Value ()))
consumeIn (Value () inParts) value
  = foldM (flip consumeInPartInMatch) (emptyMatch value) inParts

consumePartInMatch :: (NormReducePart p)
                   => In.Part ()
                   -> Match (GenValue p ())
                   -> UResult (Match (GenValue p ()))
consumePartInMatch = matchAgainF . consumePartInValue

consumeInPartInMatch :: In.Part ()
                     -> Match (In.Value ())
                     -> UResultT [] (Match (In.Value ()))
consumeInPartInMatch = matchAgainF . consumeInPartInValue

consumePartInValue :: (NormReducePart p)
                   => In.Part ()
                   -> GenValue p ()
                   -> UResult (Match (GenValue p ()))
consumePartInValue (In.PartPrim inPrim) (Value () parts)
  | inPart `notElem` parts = Failure ()
  | otherwise
  = Success Match
  { matched = Value () [inPart]
  , leftover = Value () $ inPart `delete` parts
  }
  where inPart = primToPart Proxy inPrim
consumePartInValue (In.PartPrimType inType) (Value () parts)
  = Value () <<$>> consumePrimTypeInParts inType parts
consumePartInValue (In.PartRecord inRec) (Value () parts)
  = Value () <<$>> consumeRecordInParts inRec parts

consumeInPartInValue :: In.Part ()
                     -> In.Value ()
                     -> UResultT [] (Match (In.Value ()))
consumeInPartInValue (In.PartPrim inPrim) (Value () parts)
  = Value () <<$>> consumeInPrimInParts inPrim parts
consumeInPartInValue (In.PartPrimType inType) (Value () parts)
  = hoist $ Value () <<$>> consumeInPrimTypeInParts inType parts
consumeInPartInValue (In.PartRecord inRec) (Value () parts)
  = Value () <<$>> consumeInRecordInParts inRec parts

consumeInPrimInParts :: Prim ()
                     -> [In.Part ()]
                     -> UResultT [] (Match [In.Part ()])
consumeInPrimInParts _ [] = mkUFailureT
consumeInPrimInParts inPrim (part : parts)
  = bindStackOuter continue $ consumeInPrimInPart inPrim part
  where continue (Failure ())
            = mapLeftover (part :)
          <$> consumeInPrimInParts inPrim parts
        continue (Success ())
          = mkSuccessT Match
          { matched = [In.PartPrim inPrim]
          , leftover = parts
          }

consumeInPrimInPart :: Prim ()
                    -> In.Part ()
                    -> UResultT [] ()
consumeInPrimInPart inPrim (In.PartPrim prim)
  | inPrim == prim = mkSuccessT ()
  | otherwise = mkUFailureT
consumeInPrimInPart inPrim (In.PartPrimType ptype)
  | inPrim `isPrimInstanceOf` ptype = ResultT [Success (), Failure ()]
  | otherwise = mkUFailureT
consumeInPrimInPart _ (In.PartRecord _) = mkUFailureT

consumePrimTypeInParts :: (NormReducePart p)
                       => PrimType ()
                       -> [p ()]
                       -> UResult (Match [p ()])
consumePrimTypeInParts _ [] = Failure ()
consumePrimTypeInParts inType (part : parts)
  = case consumePrimTypeInPart inType part of
         Failure ()
            -> mapLeftover (part :)
           <$> consumePrimTypeInParts inType parts
         Success ()
           -> Success Match
            { matched = [part]
            , leftover = parts
            }

consumeInPrimTypeInParts :: PrimType ()
                         -> [In.Part ()]
                         -> UResult (Match [In.Part ()])
consumeInPrimTypeInParts _ [] = Failure ()
consumeInPrimTypeInParts inType (part : parts)
  = case consumeInPrimTypeInPart inType part of
         Failure ()
            -> mapLeftover (part :)
           <$> consumeInPrimTypeInParts inType parts
         Success ()
           -> Success Match
            { matched = [part]
            , leftover = parts
            }

consumeInPrimTypeInPart :: PrimType ()
                        -> In.Part ()
                        -> UResult ()
consumeInPrimTypeInPart inType (In.PartPrim prim)
  | prim `isPrimInstanceOf` inType = Success ()
  | otherwise = Failure ()
consumeInPrimTypeInPart inType (In.PartPrimType ptype)
  | ptype == inType = Success ()
  | otherwise = Failure ()
consumeInPrimTypeInPart _ (In.PartRecord _) = Failure ()

consumePrimTypeInPart :: (NormReducePart p)
                      => PrimType ()
                      -> p ()
                      -> UResult ()
consumePrimTypeInPart inType part
  = case partToPrim part of
         Nothing -> Failure ()
         Just prim
           | prim `isPrimInstanceOf` inType -> Success ()
           | otherwise -> Failure ()

consumeRecordInParts :: (NormReducePart p)
                     => In.Record ()
                     -> [p ()]
                     -> UResult (Match [p ()])
consumeRecordInParts _ [] = Failure ()
consumeRecordInParts inRec (part : parts)
  = case consumeRecordInPart inRec part of
         Failure ()
            -> mapLeftover (part :)
           <$> consumeRecordInParts inRec parts
         Success (Match partMatch partLeftover)
           -> Success Match
            { matched = maybeToList partMatch
            , leftover = partLeftover ?: parts
            }

consumeInRecordInParts :: In.Record ()
                       -> [In.Part ()]
                       -> UResultT [] (Match [In.Part ()])
consumeInRecordInParts _ [] = mkUFailureT
consumeInRecordInParts inRec (part : parts)
  = bindStackOuter continue $ consumeInRecordInPart inRec part
  where continue (Failure ())
            = mapLeftover (part :)
          <$> consumeInRecordInParts inRec parts
        continue (Success (Match partMatch partLeftover))
          = mkSuccessT Match
          { matched = maybeToList partMatch
          , leftover = partLeftover ?: parts
          }

consumeRecordInPart :: (NormReducePart p)
                    => In.Record ()
                    -> p ()
                    -> UResult (Match (Maybe (p ())))
consumeRecordInPart inRec part
  = case partToRec part of
         Nothing -> Failure ()
         Just record -> recToPart Proxy <<<$>>> consumeRecord inRec record

consumeInRecordInPart :: In.Record ()
                      -> In.Part ()
                      -> UResultT [] (Match (Maybe (In.Part ())))
consumeInRecordInPart _ (In.PartPrim _) = mkUFailureT
consumeInRecordInPart _ (In.PartPrimType _) = mkUFailureT
consumeInRecordInPart inRec (In.PartRecord record)
  = In.PartRecord <<<$>>> consumeInRecord inRec record

consumeRecord :: (NormReducePart p)
              => In.Record ()
              -> PartRecord p ()
              -> UResult (Match (Maybe (PartRecord p ())))
consumeRecord (Record () inRecHead inRecProps) (Record () recHead recProps)
        | inRecHead /= recHead = Failure ()
        | otherwise
        = Record () recHead
  <<<$>>> bimapMatch Just justIfNonEmptyList
      <$> consumeProperties inRecProps recProps

consumeInRecord :: In.Record ()
                -> In.Record ()
                -> UResultT [] (Match (Maybe (In.Record ())))
consumeInRecord (Record () inRecHead inRecProps) (Record () recHead recProps)
        | inRecHead /= recHead = mkUFailureT
        | otherwise
        = Record () recHead
  <<<$>>> bimapMatch Just justIfNonEmptyList
      <$> consumeInProperties inRecProps recProps

consumeProperties :: (NormReducePart p)
                  => [In.Property ()]
                  -> [PartProperty p ()]
                  -> UResult (Match [PartProperty p ()])
consumeProperties _ [] = Success $ pure []
consumeProperties inProps (prop : props)
  = case consumePropertiesInProperty inProps prop of
         Failure () -> Failure ()
         Success match
            -> addPropValMatch prop match
           <$> consumeProperties inProps props

consumeInProperties :: [In.Property ()]
                    -> [In.Property ()]
                    -> UResultT [] (Match [In.Property ()])
consumeInProperties _ [] = mkSuccessT $ pure []
consumeInProperties inProps (prop : props)
  = bindStackOuter continue $ consumeInPropertiesInProperty inProps prop
  where continue (Failure ()) = mkUFailureT
        continue (Success match)
            = addPropValMatch prop match
          <$> consumeInProperties inProps props

consumePropertiesInProperty :: (NormReducePart p)
                            => [In.Property ()]
                            -> PartProperty p ()
                            -> UResult (Match (Maybe (GenValue p ())))
consumePropertiesInProperty inProps (Property () propKey propVal)
  = case glookupForce propKey inProps of
         In.NothingValue
           -> Success Match
            { matched = Just propVal
            , leftover = Nothing
            }
         In.JustValue inPropVal
            -> bimapMatch Just justIfNonEmptyVal
           <$> consume inPropVal propVal

consumeInPropertiesInProperty :: [In.Property ()]
                              -> In.Property ()
                              -> UResultT [] (Match (Maybe (In.OptValue ())))
consumeInPropertiesInProperty inProps (Property () propKey propVal)
  = case glookupForce propKey inProps of
         In.NothingValue
           -> mkSuccessT Match
            { matched = Just propVal
            , leftover = Nothing
            }
         In.JustValue inPropVal
            -> bimapMatch Just justIfNonEmptyOptVal
           <$> consumeInOpt inPropVal propVal

consumeInOpt :: In.Value ()
             -> In.OptValue ()
             -> UResultT [] (Match (In.OptValue ()))
consumeInOpt input' In.NothingValue = ResultT
  [ -- consumeIn input' input'
    Success Match
    { matched = In.JustValue input'
    , leftover = mempty -- In.JustValue $ Value () []
    }
  , Failure ()
  ]
consumeInOpt input' (In.JustValue x)
  = In.JustValue <<$>> consumeIn input' x

addPropValMatch :: GenProperty v ()
                -> Match (Maybe (v ()))
                -> Match [GenProperty v ()]
                -> Match [GenProperty v ()]
addPropValMatch (Property () propKey _) propVal props
  = (?:) <$> prop <*> props
  where prop = Property () propKey <<$>> propVal

justIfNonEmptyOptVal :: In.OptValue () -> Maybe (In.OptValue ())
justIfNonEmptyOptVal In.NothingValue = Just In.NothingValue
justIfNonEmptyOptVal (In.JustValue x) = In.JustValue <$> justIfNonEmptyVal x

justIfNonEmptyVal :: GenValue v () -> Maybe (GenValue v ())
justIfNonEmptyVal x
  | isEmpty x = Nothing
  | otherwise = Just x

justIfNonEmptyList :: [a] -> Maybe [a]
justIfNonEmptyList x
  | null x = Nothing
  | otherwise = Just x

-- | Resolves the property paths in the output value using the given
-- value, and combines the result with the given value.
produce :: (NormReducePart p)
        => Out.Value ()
        -> Match (GenValue p ())
        -> GenValue p ()
produce output' match
  = leftover match <> forceResolve output' (matched match)

produceIn :: Out.Value ()
          -> Match (In.Value ())
          -> (In.Value (), PropTranses)
produceIn output' match
  = (produceInMain output' match, transProps output' $ matched match)

-- | Resolves the property paths in the output value using the given
-- value, and combines the result with the given value.
produceInMain :: Out.Value () -> Match (In.Value ()) -> In.Value ()
produceInMain output' match
  = leftover match <> forceResolveIn' output' (matched match)
