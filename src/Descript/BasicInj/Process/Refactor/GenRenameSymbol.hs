module Descript.BasicInj.Process.Refactor.GenRenameSymbol
  ( GenRenameSymbol (..)
  , renameSymbol
  ) where

import Descript.Misc
import Control.Monad.Trans.Class
import Control.Monad.Trans.Writer.Strict

data GenRenameSymbol a = GenRenameSymbol (a ()) (a ())

renameSymbol :: (Printable a, Eq (a ())) => GenRenameSymbol a -> RefactorFunc a
renameSymbol (GenRenameSymbol old new) sym
  | sym =@= old = pure $ ann' <$ new
  | sym =@= new = sym <$ lift (tell [SymConflict ann $ pprintStr new])
  | otherwise = pure sym
  where ann' = taint ann
        ann = getAnn sym
