{-# LANGUAGE TypeFamilies #-}

module Descript.BasicInj.Process.Refactor.RenameRecord
  ( renameRecord
  , renameRecord'
  ) where

import Descript.BasicInj.Process.Refactor.GenRenameSymbol
import qualified Descript.BasicInj.Traverse.Term as T
import Descript.BasicInj.Traverse
import Descript.BasicInj.Data
import Descript.Misc
import Data.Functor.Identity
import Control.Monad.Trans.Writer.Strict

newtype RenameRecord = RenameRecord (GenRenameSymbol FSymbol)

instance Traversal RenameRecord where
  type Eff RenameRecord = ResultT RefactorError (WriterT [RefactorWarning] Identity)
  type TAnn RenameRecord = SrcAnn

  tonTerm T.RecordHead (RenameRecord rsym) sym = renameSymbol rsym sym
  tonTerm _ _ x = pure x

-- | Replaces every occurrence of the first record head with the second.
renameRecord :: String -> String -> RefactorFunc Source
renameRecord old new src = renameRecord' T.Source (mkFSymbol' old) (mkFSymbol' new) src
  where mkFSymbol' = mkFSymbol (sourceScope src) . Symbol ()

-- | Replaces every occurrence of the first record head with the second.
renameRecord' :: TTerm t -> FSymbol () -> FSymbol () -> RefactorFunc t
renameRecord' term old new = travTerm term $ RenameRecord $ GenRenameSymbol old new
