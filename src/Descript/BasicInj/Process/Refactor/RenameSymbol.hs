{-# LANGUAGE TypeFamilies #-}

module Descript.BasicInj.Process.Refactor.RenameSymbol
  ( renameSymbolAt
  ) where

import Descript.BasicInj.Process.Refactor.RenameModuleElem
import Descript.BasicInj.Process.Refactor.RenameRecord
import Descript.BasicInj.Process.Refactor.RenameProp
import Descript.BasicInj.Process.Inspect
import qualified Descript.BasicInj.Traverse.Term as T
import Descript.BasicInj.Traverse
import Descript.BasicInj.Data
import Descript.Misc

-- | Replaces every occurrence of the symbol at the given location.
-- Fails if there's no symbol at the location.
renameSymbolAt :: Loc -> String -> RefactorFunc Source
renameSymbolAt oldLoc new = renameSymbolAt' T.Source oldLoc $ Symbol () new

-- | Replaces every occurrence of the symbol at the given location.
-- Fails if there's no symbol at the location.
renameSymbolAt' :: TTerm t -> Loc -> Symbol () -> RefactorFunc t
renameSymbolAt' term oldLoc new src
  = case symbolAt' term oldLoc src of
         Nothing -> mkFailureT RefactorNoSymbolAtLoc
         Just oldEnv -> renameSymbolInEnv' term oldEnv_ new src
           where oldEnv_ = remAnns oldEnv

-- | Replaces every occurrence of the first symbol in the same
-- environment with the second.
renameSymbolInEnv' :: TTerm t -> SymbolEnv () -> Symbol () -> RefactorFunc t
renameSymbolInEnv' term (EnvModulePathElem prevs old) new
  = renameModuleElem' term prevs old new
renameSymbolInEnv' term (EnvRecordHead oldf) new
  = renameRecord' term oldf newf
  where newf = FSymbol (fsymbolScope oldf) new
renameSymbolInEnv' term (EnvPropertyKey head' old) new
  = renameProp' term head' old new
