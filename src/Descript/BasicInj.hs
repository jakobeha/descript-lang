-- | Basic implementation with injections - simple but inefficient, unchecked.
-- Allows for operations on primitives. For example, you can define a
-- function which takes 2 primitives encoding numbers, and returns a new
-- primitive encoding their sum.
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
