-- | Miscellaneous, general-purpose structures and functions which are
-- needed to use this library, but could probably be in a base library.
{-# OPTIONS_GHC -F -pgmF autoexporter #-}
