module Core.Data.Set
  ( concatMap
  , mapMaybe
  , traverse
  ) where

import qualified Data.Maybe as List (mapMaybe)
import qualified Data.List as List (concatMap)
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Prelude as List (traverse)
import Prelude hiding (concatMap, traverse)

concatMap :: (Ord b) => (a -> [b]) -> Set a -> Set b
concatMap f = Set.fromList . List.concatMap f . Set.toList

mapMaybe :: (Ord b) => (a -> Maybe b) -> Set a -> Set b
mapMaybe f = Set.fromList . List.mapMaybe f . Set.toList

traverse :: (Ord b, Applicative w) => (a -> w b) -> Set a -> w (Set b)
traverse f = fmap Set.fromList . List.traverse f . Set.toList
