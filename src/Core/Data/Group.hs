module Core.Data.Group
  ( Subtract (..)
  , Group (..)
  ) where

-- | Can be combined and subtracted.
-- Not an algebraic group, since there's no @invert@ - you can only
-- subtract values.
class (Monoid a) => Subtract a where
  -- | Subtract the second value from the first.
  (\\) :: a -> a -> a

-- | A 'Monoid' with a "negative" value. Adding a value is like
-- subtracting its negative, and vice versa. The negative of empty is empty.
class (Subtract a) => Group a where
  -- | The "negative" of this value.
  invert :: a -> a
