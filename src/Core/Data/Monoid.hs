{-# LANGUAGE DefaultSignatures #-}

module Core.Data.Monoid
  ( Monoid1 (..)
  ) where

import Data.Semigroup

-- | Is a 'Monoid' if its type variable is a 'Monoid',
class Monoid1 a where
  mempty1 :: (Semigroup b, Monoid b) => a b
  default mempty1 :: (Semigroup b, Monoid b, Monoid (a b)) => a b
  mempty1 = mempty
  mappend1 :: (Semigroup b, Monoid b) => a b -> a b -> a b
  default mappend1 :: (Semigroup b, Monoid b, Monoid (a b)) => a b -> a b -> a b
  mappend1 = mappend
