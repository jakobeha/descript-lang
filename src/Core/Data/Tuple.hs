module Core.Data.Tuple
  ( mapAll
  ) where

-- | Transforms both items in a 2-tuple.
mapAll :: (a -> b) -> (a, a) -> (b, b)
mapAll f (x, y) = (f x, f y)
