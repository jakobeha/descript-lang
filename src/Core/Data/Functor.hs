module Core.Data.Functor
       ( fmap2
       , fmap3
       , (<<$>>)
       , (<<<$>>>) ) where

-- | @fmap . fmap@.
fmap2 :: (Functor wo, Functor wi)
      => (a -> b) -> wo (wi a) -> wo (wi b)
fmap2 = fmap . fmap

-- | @fmap . fmap . fmap@.
fmap3 :: (Functor w1, Functor w2, Functor w3)
      => (a -> b) -> w1 (w2 (w3 a)) -> w1 (w2 (w3 b))
fmap3 = fmap . fmap . fmap

-- | @fmap . fmap@.
infixl 4 <<$>>
(<<$>>) :: (Functor wo, Functor wi)
        => (a -> b) -> wo (wi a) -> wo (wi b)
(<<$>>) = fmap2

-- | @fmap . fmap . fmap@.
infixl 4 <<<$>>>
(<<<$>>>) :: (Functor w1, Functor w2, Functor w3)
          => (a -> b) -> w1 (w2 (w3 a)) -> w1 (w2 (w3 b))
(<<<$>>>) = fmap3
