{-# LANGUAGE OverloadedStrings #-}

module Core.Data.Text
  ( lines'
  , unlines'
  , endPos
  , indentBullet
  , indentBlockQuote
  ) where

import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as Text

-- | Separates the text by newlines.
-- Unlike (regular) @lines@,
-- if there's a trailing newline in the text,
-- this will include an empty string at the end.
lines' :: Text -> [Text]
lines' = Text.splitOn "\n"

-- | Joins the texts with a newline.
-- Unlike (regular) @unlines@,
-- this doesn't add a trailing newline at the end.
unlines' :: [Text] -> Text
unlines' = Text.intercalate "\n"

-- | The line and column length of the text.
endPos :: Text -> (Int, Int)
endPos text = (endLine, endCol)
  where endCol = Text.length lastLine
        lastLine = last lines''
        endLine = length lines'' - 1
        lines''
          = case lines' text of
                 [] -> [Text.empty]
                 someLines -> someLines

-- | Formats into a Markdown-style bulleted list item.
indentBullet :: Text -> Text
indentBullet subPr = "- " <> indent subPr

-- | Formats into a Markdown-style block quote.
indentBlockQuote :: Text -> Text
indentBlockQuote contentPr = "> " <> indentBlockQuoteRest contentPr

indent :: Text -> Text
indent = Text.replace "\n" "\n  "

indentBlockQuoteRest :: Text -> Text
indentBlockQuoteRest = Text.replace "\n" "\n> "
