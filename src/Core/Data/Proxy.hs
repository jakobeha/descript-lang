module Core.Data.Proxy
  ( proxyOf
  ) where

import Data.Proxy

-- | A proxy for the type of the value.
proxyOf :: a -> Proxy a
proxyOf _ = Proxy
