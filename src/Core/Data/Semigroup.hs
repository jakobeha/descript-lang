{-# LANGUAGE DefaultSignatures #-}

module Core.Data.Semigroup
  ( Semigroup1 (..)
  ) where

import Data.Semigroup

-- | Is a 'Semigroup' if its type variable is a 'Semigroup',
class Semigroup1 a where
  sappend1 :: (Semigroup b) => a b -> a b -> a b
  default sappend1 :: (Semigroup b, Semigroup (a b)) => a b -> a b -> a b
  sappend1 = (<>)
