module Core.DontForce
  ( dontForce
  ) where

  -- | An expression which shouldn't be forced (evaluated) - an error.
  dontForce :: a
  dontForce = error "oops, this shouldn't be forced"
