module Core.System.FilePath
  ( takeDirectoryN
  ) where

import Core.Data.List
import System.FilePath

-- | Removes @n@ end elements from the path. Equivalent to calling
-- 'takeDirectory' @n@ times, but more efficient.
takeDirectoryN :: Int -> FilePath -> FilePath
takeDirectoryN n = joinPath . dropEnd n . splitPath
