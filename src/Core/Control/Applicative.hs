module Core.Control.Applicative
       ( pure2
       , pure3
       , (<<*>>)
       , (<<<*>>>) ) where

-- | Lifts 'pure' to 2 applicatives.
pure2 :: (Applicative wa, Applicative wb) => a -> wa (wb a)
pure2 = pure . pure

-- | Lifts 'pure' to 3 applicatives.
pure3 :: (Applicative wa, Applicative wb, Applicative wc) => a -> wa (wb (wc a))
pure3 = pure2 . pure

-- | Lifts '<*>' to 2 applicatives.
infixr 3 <<*>>
(<<*>>) :: (Applicative wa, Applicative wb) => wa (wb (a -> b)) -> wa (wb a) -> wa (wb b)
f <<*>> x = (<*>) <$> f <*> x

-- | Lifts '<*>' to 3 applicatives.
infixr 3 <<<*>>>
(<<<*>>>) :: (Applicative wa, Applicative wb, Applicative wc) => wa (wb (wc (a -> b))) -> wa (wb (wc a)) -> wa (wb (wc b))
f <<<*>>> x = (<<*>>) <$> f <*> x
