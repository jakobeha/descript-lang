module Core.Control.Monad.ST
  ( stToMio
  ) where

import Control.Monad.ST
import Control.Monad.IO.Class

-- | Converts an ST monad to a MonadIO instance.
stToMio :: (MonadIO m) => ST RealWorld a -> m a
stToMio = liftIO . stToIO
