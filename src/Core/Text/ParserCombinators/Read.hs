module Core.Text.ParserCombinators.Read
  ( forceRead
  ) where

-- | Evaluates the reader on the input, expects exactly 1 result which
-- consumes all input, rna returns that result.
forceRead :: ReadS a -> String -> a
forceRead read' input
  = case read' input of
         [(out, "")] -> out
         _ -> error $ "Expected to read exactly 1 value from " ++ input
